# project: 2020_XomeTox_Data_Analysis
# author: Sebastian Canzler
# date: Sep 22, 2021


destination_path: /data/bioinf/projects/data/2020_XomeTox_Transcriptomics/short_RNAseq/uap

# NOTE: Please substitute <EMAIL> with your EMAIL adress
# NOTE: Do not specify email options when array jobs are used!
# The latter would be the case when uap is applied with the submit-to-cluster option
cluster:
  default_submit_options: "--cpus-per-task=#{CORES} --time=24:00:00 --mem-per-cpu=8G"
  default_job_quota: 80
  singularity_container: "/global/apps/uap/uap_slurm.sif"


# Singularity Container: "/global/apps/uap/uap_slurm.sif"
# Commit: f0d2cc25834d7136fd3cad8c9300e17aaad73938
# https://github.com/yigbt/uap/commit/f0d2cc25834d7136fd3cad8c9300e17aaad73938
# Download from sylabs.io: library://boll3/default/uap.sif:sha256.fb7a846ecc3566b058cbf5f2441e18e6b0e901b5e72541ff1eb04721454cc764

constants:
  # full path to the flowcells that contain the demultilexed reads
  # PLEASE ADJUST
  - &fc1 "/data/bioinf/projects/data/2020_XomeTox_Transcriptomics/short_RNAseq/fastq/"
  # paired end sequencing
  - &paired_end False
  # temporary sort directories
  - &tsort_pos "/work/canzler/temp/xometox/"
  - &tsort_name "/work/canzler/temp/xometox/"
  - &tsort_merge "/work/canzler/temp/xometox/"
  # Illumina sequencing adapters
  - &adapters "/data/bioinf/projects/data/2017_LINA_RNAseq_uap/RNAseq_primers_index.fasta"
  # genome stuff
  - &genome "/data/bioinf/projects/data/2020_XomeTox_Transcriptomics/genome/Rattus_norvegicus.Rnor_6.0.dna.toplevel.fa"
  - &genome_annotation "/data/bioinf/projects/data/2020_XomeTox_Transcriptomics/genome/Rattus_norvegicus.Rnor_6.0.102.gtf"
  - &stringtie_annotation "/data/bioinf/projects/data/2020_XomeTox_Transcriptomics/uap/merge_assembly/merge/merge_merge_assembly.allTranscripts.gtf"
  - &cufflinks_annotation "/data/bioinf/projects/data/2020_XomeTox_Transcriptomics/uap/merge_assembly2/merge/merge_merge_assembly.allTranscripts.gtf"
  - &ht2-idx "/data/bioinf/projects/data/2020_XomeTox_Transcriptomics/hisat2_index/Rnor_6.0"
  - &genome_segemehl_faidx "/data/bioinf/projects/data/2020_XomeTox_Transcriptomics/genome/Rattus_norvegicus.Rnor_6.0.dna.toplevel.fai"


steps:

  ##############################################################################
  # Source steps

  # 0. introduce the fastq files into analysis

  fc1 (fastq_source):
    pattern: '/data/bioinf/projects/data/2020_XomeTox_Transcriptomics/short_RNAseq/fastq/[SL]*fastq.gz'
    group: '([SL]\d+)_S\d+_R1_001.fastq.gz'
    paired_end: *paired_end
    first_read: '_R1_'
#    second_read: '_R2_'


  ##############################################################################
  # Processing steps


  ### 1. quality control

  fastqc_initial (fastqc):
    _depends: fc1
    _volatile: yes

  fastx_quality_stats:
    _depends: fc1
    _volatile: yes



  ### 2. remove adapters

  cutadapt:
    _depends: fc1
    _cluster_submit_options: "--time=12:00:00 --mem-per-cpu=8G"
    dd-blocksize: "4096"
#    adapter-R2: "AGATCGGAAGAG"
    adapter-R1: "AGATCGGAAGAG"
    adapter-type: "-a"
    # keep only the leftmost string w/o spaces of the QNAME field of the FASTQ data
    fix_qnames: false
    use_reverse_complement: false
    _volatile: yes

  # cutadapt may remove only one of the two mates of a read pair
  # this causes problems for subsequent analysis
  # --> we now remove the remaining mate read

#  fix_cutadapt:
#    _depends: cutadapt
#    _cluster_submit_options: "--cpus-per-task 4 --time=12:00:00 --mem-per-cpu=6G"
#    dd-blocksize: "4096"



  ### 3. quality control

  fastqc_trimmed (fastqc):
    _depends: cutadapt 

  fastx_quality_stats_trimmed (fastx_quality_stats):
    _depends: cutadapt 




#  ### 4. map reads to genome
#  hisat2:
#    _depends: fix_cutadapt
#    _cluster_submit_options: "--cpus-per-task=12 --time=16:00:00 --mem-per-cpu=10G"
#    index: *ht2-idx
#    cores: 12
#    rna-strandness: "RF"
#    dta: true
#
#
#
#
#  ### 5. sort by genomic position
#  samtools_filter_sort_pos (samtools):
#    _depends: hisat2
#    _cluster_submit_options: "--cpus-per-task=8 --time=24:00:00 --mem-per-cpu=10G"
#    keep_header: true
#    output_bam: true
#    genome-faidx: *genome_segemehl_faidx
#    q_mapq: 20 # skip reads with map quality lower than 20, consider skipping this!
#    f_keep: 3 # SAM flags '1' and '2' set => read is paired, read mapped in proper pair
#    # sort
#    sort-by-name: false
#    temp-sort-dir: *tsort_pos
#
#
#
#
#  ### 6. sort by name (needed for htseq_count step later)
#  # sort by name on filtered BAM file version since this is smaller..
#  # no need to filter again..
#  samtools_sort_name (samtools):
#    _depends: samtools_filter_sort_pos
#    _cluster_submit_options: "--cpus-per-task=8	--time=24:00:00	--mem-per-cpu=10G"
#    keep_header: true
#    output_bam: true
#    genome-faidx: *genome_segemehl_faidx
#    # sort
#    sort-by-name: true
#    temp-sort-dir: *tsort_name
#
#
#
#
#  ### 7. assemble new transcripts
#  stringtie:
#    _depends: samtools_filter_sort_pos
#    _cluster_submit_options: "--cpus-per-task=8 --time=24:00:00 --mem-per-cpu=8G"
#    dd-blocksize: "4M"
#    p: 7 # additional CPUs to the one that is used anyway
#    G: *genome_annotation
#    rf: True
#
#
#
#
#  ### 8. merge assemblies
#  stringtieMerge:
#    _depends: stringtie
#
#
#
#
#  ### 9. annotate transcript assembly
#  cuffcompare:
#    _depends: stringtieMerge
#    _cluster_submit_options: "--cpus-per-task=1 --time=8:00:00 --mem-per-cpu=8G"
#    r: *genome_annotation
#    s: *genome
#    run_id: "merge"
#
#
#
#
#  ### 10. filter merged transcripts
#  # class possibilities
#  #    1 = Complete match of intron chain
#  #    2 c Contained
#  #    3 j Potentially novel isoform (fragment): at least one splice junction is shared with a reference transcript
#  #    4 e Single exon transfrag overlapping a reference exon and at least 10 bp of a reference intron, indicating a possible pre-mRNA fragment.
#  #    5 i A transfrag falling entirely within a reference intron
#  #    6 o Generic exonic overlap with a reference transcript
#  #    7 p Possible polymerase run-on fragment (within 2Kbases of a reference transcript)
#  #    8 r Repeat. Currently determined by looking at the soft-masked reference sequence and applied to transcripts where at least 50% of the bases are lower case
#  #    9 u Unknown, intergenic transcript
#  #    10 x Exonic overlap with reference on the opposite strand
#  #    11 s An intron of the transfrag overlaps a reference intron on the opposite strand (likely due to read mapping errors)
#  #    12 . (.tracking file only, indicates multiple classifications)
#  filter_class_codes (post_cufflinksSuite):
#    _depends: cuffcompare
#    _cluster_submit_options: "--cpus-per-task=1 --time=8:00:00 --mem-per-cpu=8G"
#    remove-gencode: true
#    remove-unstranded: true
#    remove-by-gene-name: false
#    class-list: "c,j,=,e,o,p,r,s,." # 2B removed
#    filter-by-class: true
#    filter-by-class-and-gene-name: false
#    run_id: "merge"
#
#
#
#
#  ### 11. merge annotation and new assembled transcripts
#  # returns a gtf file containing all new transcripts and the annotation
#  # new step that merges and sorts the final file
#  merge_assembly:
#    _depends: filter_class_codes
#    _cluster_submit_options: "--cpus-per-task=1 --time=8:00:00 --mem-per-cpu=4G"
#    reference: *genome_annotation
#    temp-sort-dir: *tsort_merge
#
#
#
#
#  ### 12. counting of feature hits
#  ### a) use htseq_count
#  htseq_count:
#    _depends:
#      - samtools_sort_name
#      - merge_assembly
#    _cluster_submit_options: "--cpus-per-task=4 --time=24:00:00 --mem-per-cpu=14G"
#    order: "name"
#    stranded: "reverse"
#    mode: "intersection-strict"
#    type: "exon"
#    idattr: "gene_id"
#    dd-blocksize: "4M"
#    pigz-blocksize: "4096"
#

tools:

##############################################################################

  # External tools

  # URL: http://zlib.net/pigz/
  pigz:
    path: 'pigz'
    get_version: '--version'
    exit_code: 0

# URL: http://www.bioinformatics.babraham.ac.uk/projects/fastqc/
  fastqc:
    path: 'fastqc'
    get_version: '--version'
    exit_code: 0

  fastx_quality_stats:
    path: 'fastx_quality_stats'
    get_version: '-h'
    exit_code: 1

  # URL: https://github.com/marcelm/cutadapt
  cutadapt:
    path: 'cutadapt'
    get_version: '--version'
    exit_code: 0

  hisat2:
    path: 'hisat2'
    get_version: '--version'
    exit_code: 0

  stringtie:
    path: 'stringtie'
    get_version: '--version'
    exit_code: 0

  htseq-count:
    path: 'htseq-count'
    get_version: '-h'
    exit_code: 0

  cuffcompare:
    path: 'cuffcompare'
    get_version: '-h'
    exit_code: 1

  cufflinks:
    path: 'cufflinks'
    get_version: '--version'
    exit_code: 1

  cuffmerge:
    path: 'cuffmerge'
    get_version: '--version'
    exit_code: 0

  feature_counts:
    path: 'featureCounts'
    get_version: '-v'
    exit_code: 0
    

##############################################################################

  # Internal tools

  fix_cutadapt:
    path: '/opt/uap/tools/fix_cutadapt.py'
    get_version: '--version'
    exit_code: 0

  fix_qnames:
    path: '/opt/uap/tools/fix_qnames.py'
    get_version: '-h'
    exit_code: 0

  samtools:
    path: 'samtools'
    get_version: ''
    exit_code: 1

##############################################################################

  # Unix tools

  cat:
    path: 'cat'
    get_version: '--version'
    exit_code: 0

  dd:
    path: 'dd'
    get_version: '--version'
    exit_code: 0

  mkfifo:
    path: 'mkfifo'
    get_version: '--version'
    exit_code: 0

  mkdir:
    path: 'mkdir'
    get_version: '--version'
    exit_code: 0

  mv:
    path: 'mv'
    get_version: '--version'
    exit_code: 0

  printf:
    path: 'printf'
    get_version: '--version'
    exit_code: 0

  tar:
    path: 'tar'
    get_version: '--version'
    exit_code: 0

  sort:
    path: 'sort'
    get_version: '--version'
    exit_code: 0
