# CEFIC LRI - XomeTox

The XomeTox project aims at evaluating multi-omics data integration for assessing rodent thyroid toxicity.

The results of this integration effort to simultaneous analyse transcriptomics, proteomics, metabolomics as well as clinical and histopathological data are summarized in our paper 'Multi-omics data integration in toxicology' (currently in preparation).

The stage was set with our recent opinion article where we derived prospects and challenges for the utilization of multi-omics data in systems toxicology and regulatory risk assessment, see [Canzler _et al._ (2020)](https://doi.org/10.1007/s00204-020-02656-y).

Here, in this repository we summarize the Rmarkdown files that have been used for omics data pre-processing and the subsequent multi-omics data integration.

## Raw Data Availability

**Short and long RNA-Seq**

Demultiplexed fastq files of both short and long RNA sequencing experiments were deposited at NCBI SRA with Bio-Project identifier [PRJNA695243](https://www.ncbi.nlm.nih.gov/bioproject/695243).
**Please note**, the data set is currently embargoed and will be publicly available upon the publication of the paper.

**Proteomics**

Raw files and processed results are available on PRIDE with the identifier [PXD026835](https://www.ebi.ac.uk/pride/archive/projects/PXD026835).
**Please note**, the data set is currently embargoed and will be publicly available upon the publication of the paper.

**Phosphoproteomics**

Raw files and results are available on PRIDE with the identifier [PXD030254](https://www.ebi.ac.uk/pride/archive/projectsPXD030254).

**Tissue metabolomics**

Raw and processed data sets have been deposited at Metabolomics Workbench with the StudyID ST002023.
The project can be directly accessed with the DOI [http://dx.doi.org/10.21228/M8MD8N](http://dx.doi.org/10.21228/M8MD8N) .

**Plasma metabolomics**

Plasma metabolomics data was submitted to Zenodo with the DOI [https://doi.org/10.5281/zenodo.5900664](https://doi.org/10.5281/zenodo.5900664).


# Workflows

In this repository, we collected the essential workflows that are needed to reproduce the results of our paper.

## Reproducibility

To ensure reproducibility of our workflows, we created different Docker Container.
Those container are based on the [Rocker project](https://rocker-project.org/).

To re-run the worfkows on you local machine, you need to run the particular container that is mentioned in each individual `Rmd` file.

## 00 `uap` Configuration Files

Fastq files were processed in uap, an environment for reproducible and robust
high throughput data analysis [(Kämpf et al., 2019)](https://doi.org/10.1186/s12859-019-3219-1).

- uap config file for long RNA-Seq: `00_transcriptomics_uap_config.yaml`
- uap config file for short RNA-Seq: `00_short_transcriptomics_uap_config.yaml`


## 01 Preprocessing

Pre-processing workflows of all individual omics layers are named in the format *01_omics_processing.Rmd*

For example, to re-run the transcriptomics preprocessing workflow, you need to run the `Rmd` file `01_transcriptomics_processing.Rmd`

A proprocessed html output is given for each `Rmd` file.



## 02 Single Omics Analysis

Each omics layer is analysed separately to calculate differentially altered features, fold changes and siginificance levels.

Those analysis workflows of individual omics layers are named in the format *02_omics_analysis.Rmd*

For example, to re-run the calculation of differentially abundant proteins, run the `Rmd` file `02_proteomics_DEPs.Rmd`

A proprocessed html output is given for each `Rmd` file.


## 03 MEFISTO Model Development

For our paper, we created four different multi-omics models utilizing the [`MOFA2`](https://biofam.github.io/MOFA2/) R package, depending on the target tissue (thyroid vs liver) and the administered compound (PTU vs Phenytoin).

The Rmarkdown files to build the models denoted `03_MEFISTO_model_organ_compound.Rmd`.

For example, to create the thyroid PTU model, run the `03_MEFISTO_model_thyroid_PTU.Rmd` file.

A proprocessed html output is given for each `Rmd` file.

## 04 Pathway Enrichment

We compared different strategies to calculate a pathway enrichment. We therefore
utilized results from the previous single omics analysis to calculate
differentially altered features as well as feature weights from the `MEFISTO`
models.

The Rmarkdown files to compare the different strategies are:

- `04_pathway_enrichment_liver_Phenytoin.Rmd`
- `04_pathway_enrichment_thyroid_PTU.Rmd`

## 05 Grouping of Samples

We compared the ability of single and multi-omics models to group samples into
the classes `controls`, `treated`, and `recovery`.

The Rmarkdown files to calculate the grouping procedure are:

- `05_grouping_thryoid_PTU.Rmd`
- `05_grouping_liver_Phenytoin.Rmd`

## 06 Evaluation of Omics Layers

We built different models that were missing one or two omics layers and compared
their ability to cluster samples in the classes `controls`, `treated`, and
`recovery` with the full model.

The Rmarkdown file to run the comparison is:

- `06_evaluation_omics_layer.Rmd`
