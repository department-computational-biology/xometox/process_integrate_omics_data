---
title: "Multi-omics data integration with covariates"
subtitle: "Grouping of samples - Thyroid - PTU"
author: "Sebastian Canzler"
date: "June 19th, 2024"
output:
  bookdown::html_document2:
    toc: true
    toc_float: true
    number_sections: true
    theme: united 
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

# Aim

Within this markdown we utilize single and multi-omics based approaches to group
samples.


**Prerequisites**

This workflow utilizes different preprocessed data sets

- Single omics models
- Single transcriptome model
- Multi-omics model 

# Reproducibility

To reproduce the analysis please use the following singularity container: `rocker_multiomics_analysis:latest.statmod.sif`

Or download from docker hub: `docker://boll3/rocker_multiomics:latest`

Digest: `db12fdf`

You can reproduce this Rmarkdown file and the figure by running:

```{r render-rmarkdown, eval = FALSE, engine='bash'}
cd /path/to/repo/
singularity exec /path/to/container/rocker_multiomics_analysis\:latest.statmod.sif \
  Rscript -e '{rmarkdown::render("05_grouping_thyroid_PTU.Rmd")'
```


# Set-up

## Local (R) environment

Here, all directories are set to the correct paths of this project:

```{r set-directories, echo = FALSE}
#####################################
# CHANGE WORKING DIRECTORY HERE !!! #
#####################################
my_wd <- "/path/to/workingdir/"
my_wd <- "/home/canzler/git-canzler/projects/2022_multi_omics_paper_scripts"
setwd(my_wd)


####################################
# CHANGE OUTPUT DIRECTORY HERE !!! #
####################################
data_dir <- "path/to/datadir"
data_dir <- "/home/canzler/git-canzler/projects/2022_multi_omics_paper_scripts/DATA/Grouping/05_thyroid_models"


###################################
# CHANGE RDATA DIRECTORY HERE !!! #
###################################
RdataDir <- file.path("/path/to/directory", "RData", fsep = .Platform$file.sep)
RdataDir <- file.path(data_dir, "RData", fsep = .Platform$file.sep)
if (!dir.exists(RdataDir)) {
  dir.create(RdataDir)
}


###################################
# CHANGE INPUT DIRECTORY HERE !!! #
###################################
indir <- file.path("/path/to/directory", "input", fsep = .Platform$file.sep)
indir <- file.path(data_dir, "input", fsep = .Platform$file.sep)
if (!dir.exists(indir)) {
  dir.create(indir)
}

```

- Working directory: ```r my_wd```
- Data directory on EVE: ```r data_dir```
- RData storage: ```r RdataDir```


Necessary libraries to run this analysis are the following:

```{r load-libraries, warning=FALSE, message=FALSE}
# Bioconductor stuff
library(MOFA2)

# CRAN stuff
library(tidyverse)
library(magrittr)
library(pvclust)
library(RColorBrewer)
library(dendextend)
library(factoextra)
library(stats)
```

## Functions and annotations

```{r functions}

## describe the samples prior to plotting
annotation <- data.frame( Treatment = rep( c(rep( "Control", 5), rep("Phenytoin_low", 5), 
                                        rep( "Phenytoin_high", 5), rep( "PTU_low", 5), 
                                        rep( "PTU_high", 5)),3),
                        Timepoint = c( rep( "2 weeks", 25), rep( "4 weeks", 25), rep( "6 weeks", 25)))

## function to plot the first two linear discriminants
## to visualize the separation of samples
plot_lda <- function( lda_obj, title, treatment = "ptu", subtitle = "", exclude = NULL){
  
  pred <- predict( lda_obj)
  
  if( ! treatment %in% c( "ptu", "phenytoin")){
    stop( "Treatment has to be either 'ptu' or 'phenytoin'.", call. = TRUE)
  }
  
  if( treatment == "ptu"){
    anno_sub <- annotation %>% filter( grepl( "Control|PTU", Treatment))
  } else{
    anno_sub <- annotation %>% filter( grepl( "Control|Phenytoin", Treatment))
  }
  
  if( ! is.null( exclude)){
    anno_sub <- anno_sub[-exclude,]
  }
  
  df <- data.frame( LD1 = pred$x[,1],
                    LD2 = pred$x[,2],
                    Treatment = anno_sub$Treatment,
                    Timepoint = anno_sub$Timepoint)
  
  props <- prop.table( lda_obj$svd^2)
  
  plt <- ggplot( df, aes( x = LD1, y = LD2, color = Treatment, shape = Timepoint)) +
  geom_point(size =3) + 
  scale_color_manual( breaks = c( "Control", "Phenytoin_low", "Phenytoin_high",
                                  "PTU_low", "PTU_high"),
                      values = c( "orange", "green", "green4",
                                  "royalblue2", "blue3")) +
  xlab( paste0("LD1: ", round( props[1], 2), "% separation")) +
  ylab( paste0("LD2: ", round( props[2], 2), "% separation")) +
  ggtitle( title, subtitle = subtitle) + 
  theme_bw()
  
  return( plt)
  
}

#' Calculate the min-max normalization for a given object
min_max <- function( df){
  
  min <- min( df)
  max <- max( df)
  
  return( (df - min) / (max - min))
  
}

#' Calculate the colwise min-max normalization for a given matrix of data frame 
min_max_colwise <- function( df){
  
  return( apply( df, 2, min_max))
  
}

#' Extract factor values from a given MOFA model, sort samples based on their animal number,
#' and assign treatment groups as factor
#' 
format_factor_values <- function( model, missing_samples = list()){
  
  groups <- as.factor( paste0(rep( rep( c("Controls", "Low dose", "High dose"), 3), each = 5), " ", 
                                    rep( c( "2 Weeks", "4 Weeks", "6 Weeks"), each = 15)))
  
  if( length( missing_samples) != 0){
    groups <- groups[ -missing_samples]
  }
  
  df <- as.data.frame( get_factors( model)$`1`) %>%
  mutate( samples = as.integer( gsub( ".*A([0-9]+).G.*", "\\1", rownames(.)))) %>%
  arrange( samples) %>%
  mutate( Group = groups) %>%
  dplyr::select( -c( "samples"))
  
  return( df)
  
}

evaluate_clustering <- function( groups, clustering){
  
  # At first we have to annotate the cluster derived by the clustering algorithm
  names( groups) <- names( clustering)
  ngroups <- levels( groups)
  ncluster <- sort( unique( clustering))
  
  m <- lapply( ncluster, function( c){
    
    s <-  names( clustering)[clustering==c]
    names( which.max( summary( groups[ names( groups) %in% s])))
    
  })
  m <- unlist( m)
  
  #if( length( unique( m)) != length( ngroups)){ stop( "There is a problem with the assigned cluster names\n")}
  
  # update the clustering with group names
  clustering_mapped <- m[ clustering]
  names( clustering_mapped) <- names( clustering)
  clustering_mapped <- factor( clustering_mapped, levels = ngroups)
  
  # construct confusion matrix
  d <- table( groups, clustering_mapped)
  
  # return results
  return(
    tibble( Accuracy = sum(diag(d))/sum(d),
          Incorrect_classified = 1-sum(diag(d))/sum(d),
          Cases_correct = sum(diag(d)),
          Cases_incorrect = sum(d)-sum(diag(d)),
          Control = d[1,1]/sum(d[1,]),
          Treated = d[2,2]/sum(d[2,]),
          Recovery = d[3,3]/sum(d[3,]))
  )
}
```


## Variables

Set some pre-defined variables.

```{r set-variables}

recompute <- 0
FDR <- 0.05
bootstraps <- 2000

```


## Conflict resolution

As usual, there are several packages that provide functions with identical
names. Since we are mainly interested in a function of a particular package, we
will clarify this conflict right at the beginning.

**Please note** that functions with the same name from other packages must be
called with their package name as namespace, for example `AnnotationDbi::select()`.

```{r conflicted}

select <- dplyr::select
filter <- dplyr::filter
rename <- dplyr::rename
group_by <- dplyr::group_by

```

# Thyroid PTU models

**Sample information**

```{r load-sample-information}


filename <- file.path( indir, "01_sampleTable.rda", fsep = .Platform$file.sep)

load( file = filename)


sample_metadata <- sampleTable %>% filter( Tissue == "Thyroid") %>%
  select( Sample, Condition, Treatment, Concentration, Treatment_period, Recovery_phase, Animal_number, Treatment_group) %>% 
  rename( "sample" = Sample,
          "Compound" = Treatment) %>%
  mutate( sample = gsub( "-", ".", sample))
sample_metadata$Timepoint <- c( rep( "2 Weeks", 25),
                                rep( "4 Weeks", 25),
                                rep( "6 Weeks", 25))
sample_metadata$Treatment <- rep( c(rep( "Control", 5), rep( "Phenytoin_low", 5), rep( "Phenytoin_high", 5), rep( "PTU_low", 5), rep( "PTU_high", 5)), 3)

```


**Single omics models**

Load the trained single omics models. Each `rda` file contains two models: the
PTU and the Phenytoin-based model.

```{r load-single-models}

filename <- file.path( indir, "03_thyroid_trans_mofa_model_trained.rda", 
                       fsep = .Platform$file.sep)
load( filename)

filename <- file.path( indir, "06_thyroid_prot_mofa_model_trained.rda", 
                       fsep = .Platform$file.sep)
load( filename)

filename <- file.path( indir, "08_thyroid_meta_mofa_model_trained.rda", 
                       fsep = .Platform$file.sep)
load( filename)

filename <- file.path( indir, "10_plasma_mofa_model_trained.rda", 
                       fsep = .Platform$file.sep)
load( filename)

filename <- file.path( indir, "10_thyroid_miRNA_mofa_model_trained.rda", 
                       fsep = .Platform$file.sep)
load( filename)


```


Add the sample information to each of the loaded models:

```{r add-sample-info-to-model}

samples_metadata(trans.ptu.trained) <- sample_metadata %>% filter( Compound != "Phenytoin")
samples_metadata(prot.ptu.trained) <- sample_metadata %>% filter( Compound != "Phenytoin")
samples_metadata(meta.ptu.trained) <- sample_metadata %>% filter( Compound != "Phenytoin")
samples_metadata(plasma.ptu.trained) <- sample_metadata %>% filter( Compound != "Phenytoin")
samples_metadata(miRNA.ptu.trained) <- sample_metadata %>% filter( Compound != "Phenytoin")

```

**Multi-omcis models**

```{r load-multi-models}

filename <- file.path( indir, "02_thyroid_complete_mefisto_model_trained_PTU.rda", 
                       fsep = .Platform$file.sep)
load( filename)

samples_metadata(mefisto.ptu.trained) <- sample_metadata %>% filter( Compound != "Phenytoin")
```

## Min-Max Normalization

The min-max normalization transforms a certain distribution within the range
(0,1), whereas the shape of the distribution is conserved. This sounds exactly
like what we need: with this normalization we are able to *frame* the latent
factor distributions into intervals between 0 and 1 while keeping their
characteristics.

```{r min-max-normalization}

trans_ptu_df <- format_factor_values(trans.ptu.trained)
prot_ptu_df <- format_factor_values(prot.ptu.trained)
meta_ptu_df <- format_factor_values(meta.ptu.trained)
plasma_ptu_df <- format_factor_values(plasma.ptu.trained)
miRNA_ptu_df <- format_factor_values(miRNA.ptu.trained)
multi_ptu_df <- format_factor_values(mefisto.ptu.trained)

ptu_list <- list(Transcriptomics = min_max_colwise(trans_ptu_df[,1:5]), 
                 Proteomics = min_max_colwise(prot_ptu_df[,1:5]),
                 Metabolomics = min_max_colwise(meta_ptu_df[,1:5]),
                 Plasma = min_max_colwise(plasma_ptu_df[,1:5]),
                 miRNA = min_max_colwise(miRNA_ptu_df[,1:5]),
                 Multiomics = min_max_colwise(multi_ptu_df[,1:5]))

```

# Clustering of samples

## Annotations

To evaluate the clustering later on, we need to annotate the samples with their
respective treatment group.

```{r annotations}

## Color annotation for the dendrogram
color_samples <- c( rep( "orange", 5), rep("royalblue2", 5), rep( "blue3", 5), rep( "orange", 5),
                    rep("royalblue2", 5), rep("blue3", 5), rep("orange", 5), rep("coral1", 10))

Treatment <- color_samples

# Annotation of treatment groups
groups <- factor( c( rep( "Control", 5), rep( "Low dose", 5), rep( "High dose", 5),
             rep( "Control", 5), rep( "Low dose", 5), rep( "High dose", 5),
             rep( "Control", 5), rep( "Recovery", 10)), 
             levels = c("Control", "Low dose", "High dose", "Recovery"))

groups_simple <- factor( c( rep( "Control", 5), rep( "Treated", 10),
             rep( "Control", 5), rep( "Treated", 10),
             rep( "Control", 5), rep( "Recovery", 10)),
             levels = c( "Control", "Treated", "Recovery"))

col <- brewer.pal(n = 6, name = 'Pastel1')
col <- c( "Multiomics" = "#FBB4AE", "Transcriptomics" = "#CCEBC5", "Proteomics" = "#DECBE4",
          "Metabolomics" = "#FED9A6", "Plasma" = "#E5D8BD", "miRNA" = "#F2F2F2")
```

## Hierarchical clustering

Do an actual clustering of the samples

```{r clustering}

filename <- file.path( RdataDir, "01_bootstraped_dendrograms.rda", fsep = .Platform$file.sep)

if( recompute || !file.exists( filename)){
    
  dend_list <- lapply( ptu_list, function( df){
    dend <- as.dendrogram( hclust( get_dist( df, method = "euclidean"))) 
    labels_colors( dend) <- color_samples[order.dendrogram( dend)]
    dend
  })
  
  pvc_list <-  lapply( names( ptu_list), function( name){
        pvc <- pvclust(data = t(ptu_list[[name]]), method.hclust = "complete", method.dist = "euclidean", nboot = bootstraps, iseed = 42)
  })
  names( pvc_list) <- names( ptu_list)

  save( dend_list, pvc_list, file = filename)
  
} else{
  
  load( file = filename)
  
}


```


## Evaluate clustering

To evaluate the clustering, we will make use of the simplified cluster
annoation, meaning that we have only three groups:

1. Controls
2. Treated (low and high dose)
3. Recovery


```{r evaluate-clustering}

eval <- lapply( dend_list, function( dend){
  
  evaluate_clustering(groups_simple, cutree_1k.dendrogram(dend, k = length(unique(groups_simple))))
  
})

```

Create a barplot illustrating the grouping accuracies.

```{r plot-accuracy-2, fig.align='center', fig.cap='Barplot illustrating the accuracy of the clustering for each MOFA model.', fig.width=5, fig.height=8}

col <- c( "Control" = "orange", "Treated" = "royalblue2", "Recovery" = "coral1")

df <- do.call( bind_rows, eval) %>% 
  mutate( Omics = names( eval)) %>% 
  select( Accuracy, Control, Treated, Recovery, Omics) %>% 
  rename( "Overall" = Accuracy) %>% 
  pivot_longer( cols = -Omics, names_to = "Group", values_to = "Accuracy") %>% 
  filter( Group != "Overall")

df$Treatment <- factor( df$Group, levels = c( "Control", "Treated", "Recovery"))
df$Omics[ df$Omics == "Serum"] <- "Plasma"
df$Omics <- factor( df$Omics, levels = unique( df$Omics))

barplot_acc <- ggplot( df, aes( x = Omics, y = Accuracy, fill  = Treatment)) +
  geom_bar( stat = 'identity', position = 'dodge', width = 0.5) +
  scale_fill_manual( values = col) +
  theme_bw() +
  theme( panel.grid = element_blank(),
         axis.text.x = element_text( angle = 45, vjust = 1, hjust=1),
         legend.position = 'bottom')

barplot_acc

```


## Bootstrapping approach

In order to add some more statistical support to the clustering, we can utilize
the `pvclust` package. Therein, we can calculate thousands of bootstrapped
dendrograms and calculate how often the detected clusters persists. **Please
note** that the order of the subcluster is not important!

```{r bootstrapping, fig.width=12}

plot_list <- lapply( names( pvc_list), function( name){
  
  pvc <- pvc_list[[name]]  
  dend <- dend_list[[name]]
  
  ## get internal nodes
  xy <- dend %>% get_nodes_xy()
  is_internal_node <- is.na( dend %>% get_nodes_attr("leaf"))
  is_internal_node[ which.max(xy[,2])] <- FALSE
  xy <- as.data.frame( xy[is_internal_node,])
  ## get the bootstrap support from pvclust
  xy$V3 <- pvc$edges$au[ rank( xy$V2, ties.method = "first")]
  
  
  par( mar = c(9,4.5,3,4.5) + 0.1, xpd = NA)
  plot(dend, main = name, ylab = "Height")
  rect.dendrogram( dend, k = 3, border = 8, lty = 5, lwd = 2, lower_rect = -0.38)
  text(xy[,1]-0.35, xy[,2]+0.02, labels=round( xy$V3 * 100, digits = 0), col="red", cex = 1)
  
  colors <- as.data.frame( Treatment)
  colors$Treatment[ colors$Treatment == "blue3"] <- "royalblue2"
  colored_bars( colors = colors, dend, y_shift = -0.4)
  

  # Add the legend manually
  legend("topright", legend = c('Controls', 'Treated', 'Recovery'), pch = 15, pt.cex = 1.5, cex = 1, bty = 'n',
       inset = c(-0.25, 0), # place outside
       title = "Treatment", 
      col = c('orange', 'royalblue2', 'coral1'))
  
  p <- recordPlot()
  p
  
})

```


# Session

```{r session}
sessionInfo()
```







