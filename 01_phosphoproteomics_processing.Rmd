---
title: "Preprocessing phosphoproteomics data"
author: "Sebastian Canzler"
date: "June 13th, 2024"
output:
  bookdown::html_document2:
    toc: true
    toc_float: true
    number_sections: true
    theme: united 
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```


# Aim

This Rmarkdown file covers the processing of the phosphoproteomics data in the
liver samples of the XomeTox Project.

Starting from count data in a given directory, we will mean normalize the TMT
batches, transform the data using the variance-stablizing transformation and
filter the data set according to the MOFA2 tutorial.

# Reproducibility

To reproduce the analysis please use the following singularity container: `rocker_multiomics_analysis:latest.sif`

You can download the container from docker hub: `docker://boll3/rocker_multiomics:latest`

Digest: `db12fdf`

This `.Rmd` file that is the basis of this report has been rendered via:

```{r render, eval=FALSE}
rmarkdown::render("01_proteomics_processing.Rmd", output_format = "html_document")
```


# Set-up

<p align="right"><a href="#top">&uarr; Back to top</a></p>

Here, all directories are set to the correct paths of this project. Depending on
the machine that is used, the number of CPUs are adjusted.

```{r set-directories, echo = FALSE}
#####################################
# CHANGE WORKING DIRECTORY HERE !!! #
#####################################
my_wd <- "/path/to/workingdir/"
my_wd <- "/home/canzler/git-canzler/projects/2022_multi_omics_paper_scripts"
setwd(my_wd)

data_dir <- "/home/canzler/git-canzler/projects/2022_multi_omics_paper_scripts/DATA/phosphoproteomics/01_preprocessing"

####################################
# CHANGE OUTPUT DIRECTORY HERE !!! #
####################################
# for storing .Rdata files
RdataDir <- file.path("/path/to/directory", "RData", fsep = .Platform$file.sep)
RdataDir <- file.path(data_dir, "RData", fsep = .Platform$file.sep)
if (!dir.exists(RdataDir)) {
  dir.create(RdataDir)
}


###################################
# CHANGE INPUT DIRECTORY HERE !!! #
###################################
# for storing input files
indir <- file.path("/path/to/directory", "input", fsep = .Platform$file.sep)
indir <- file.path(data_dir, "input", fsep = .Platform$file.sep)
if (!dir.exists(indir)) {
  dir.create(indir)
}
```

* Working directory: ```r my_wd```
* Data directory on EVE: ```r data_dir```
* RData storage: ```r RdataDir```


```{r load-libraries-2, warning=FALSE, results='hide', message=FALSE}
library("DEP")
library("sva")
library("limma")
library("knitr")
library("readxl")
library("xtable")
library("tidyverse")
library("magrittr")
library("stringr")
library("DT")
library("SummarizedExperiment")
library("gtools")
```


## Functions and annotations

```{r functions}
#' Function for row-wise mean normalization
#'
#' This function calculates the mean for each row of a given data.frame or
#' tibble and uses this as a normalization factor.
#'
#' @param df Data.frame or tibble that must only contain numeric columns
#'
#' @return Data.frame of the same size where each value is normalized by
#'   its row mean
#'
#' @export
mean_normalization <- function(df) {
  return(df / rowMeans(df, na.rm = TRUE))
}


#' Create a summarized Experiment object from data table
#'
#' This method re-formats a given data.frame with intensities into a summarized
#' experiment object by means of the DEP package.
#'
#' @param df Data.frame with measured intensities. The first column represents
#'   feature IDs while the remaining columns represent samples. Rows are
#'   measured features.
#'
#' @return SummarizedExperiment object.
#'
#' @importFrom DEP make_unique make_se
create_se <- function(df) {
  ## get colnames of given data frame
  label <- colnames(df)[-1]

  ## get experimental design
  exp_design <- data.frame(label,
    condition = gsub("_BR[0-9]*", "", label),
    replicate = gsub(".*_BR", "BR", label)
  )

  ## set 'name' and 'ID' column
  df_unique <- make_unique(df,
    names = "Site",
    ids = "Site",
    delim = ";"
  )

  ## create a summarized experiment from data frame
  se <- make_se(
    proteins_unique = df_unique,
    columns = 2:ncol(df),
    expdesign = exp_design
  )

  return(se)
}


#' Run variance-stabilizing normalization on a given data.frame
#'
#' This method re-formats a given data.frame with intensities into a summarized
#' experiment object by means of the DEP package.
#' A variance stabilizing normalization is subsequently applied and the
#' corresponding data.frame is returned
#'
#' @param df Data.frame with measured intensities. The first column represents
#'   feature IDs while the remaining columns represent samples. Rows are
#'   measured features.
#'
#' @return Data.frame with variance stabilized intensities.
#'
#' @importFrom DEP make_unique make_se normalize_vsn get_df_wide
#' @importFrom dplyr select
run_vsn <- function(df) {
  se <- create_se(df)
  assay(se)[is.nan(assay(se))] <- NA

  ## apply variance stabilized normalization
  norm <- normalize_vsn(se)

  ## Remove columns 'name' and 'ID' and rechange the order to put 'Accession' at first position
  df_norm <- get_df_wide(norm) %>%
    dplyr::select(-c("name", "ID")) %>%
    dplyr::select(Site, everything())

  return(df_norm)
}


reliable_identification <- function(data, reliable) {
  ## calculate the number of observations for each row
  ## filter data based on those rows where we have more
  ## than reliable observations
  df <- data %>%
    rowwise() %>%
    dplyr::mutate(re = sum(!is.na(c_across(-Site)))) %>%
    dplyr::filter(re >= reliable) %>%
    dplyr::pull(Site)

  # you only need to return the accession IDs
  return(df)
}


#' Filter a data set for features based on the number of measurements in each
#' contrast group
#'
#' Filtering procedure is done as follows: Check for each treatment group that
#' the filtered features are measured in more samples than specified in the
#' variable 'observations'. Make a unique list of all features from all
#' treatment groups and subset the given data frame to those features.
#'
#' @param data Data.frame with the data to be filtered. Columns are samples,
#'   rows are features. Accession column represents the feature name.
#' @param replicate String or pattern that is needed to identify the individual
#'   treatment groups.
#' @param observations Integer speficifying the number of samples where a
#'   certain feature has to be measured.
#'
#' @return  Data.frame with the subsetted set of features
#'
#' @importFrom dplyr filter
#'
#' @export
run_reliable <- function(data, replicate, observations) {
  # change 0 to NA
  data[data == 0] <- NA

  ## get all samples
  samples <- unique(gsub(replicate, "", colnames(data)[-1]))

  ## get list of metabolites with number of measurements >= reliable
  ## per contrast
  feature_list <- lapply(samples, function(sample) {
    columns <- colnames(data)[grep(paste0("Site|", sample), colnames(data))]
    reliable_identification(data[, columns], observations)
  })

  # make unique list of proteins
  features <- unique(unlist(feature_list))

  ## subset the original data to those proteins
  return(data %>% filter(Site %in% features))
}


#' Function to plot a PCA
#'
#' Customized color scheme for XomeTox samples
#'
#' @param mat Matrix object to calculate the PCA from
#'
#' @return ggplot object
plot_pca <- function(mat, sub = NULL) {
  pc <- prcomp(t(mat))
  percentVar <- round(pc$sdev^2 / sum(pc$sdev^2) * 100, digits = 2)
  pca_data <- data.frame(samples = rownames(pc$x), PC1 = pc$x[, 1], PC2 = pc$x[, 2])
  pca_data$Treatment <- gsub("_(short|long|recovery)_BR.*", "", pca_data$samples)
  pca_data$Timepoint <- ifelse(grepl("short", pca_data$samples), "2 Weeks", ifelse(grepl("long", pca_data$samples), "4 Weeks", "6 Weeks"))

  plt <- ggplot(pca_data, aes(x = PC1, y = PC2, color = Treatment, shape = Timepoint)) +
    geom_point(size = 3) +
    scale_color_manual(
      breaks = c(
        "Control", "Phenytoin_low", "Phenytoin_high",
        "PTU_low", "PTU_high"
      ),
      values = c(
        "orange", "green", "green4",
        "royalblue2", "blue3"
      )
    ) +
    xlab(paste0("PC1: ", percentVar[1], "% variance")) +
    ylab(paste0("PC2: ", percentVar[2], "% variance")) +
    ggtitle(paste0("PCA of proteomics data ( ", nrow(mat), " variable proteins)")) +
    theme_bw()

  if (length(sub) > 0) {
    plt <- plt +
      ggtitle(paste0("PCA of phosphoproteomics data ( ", nrow(mat), " variable sites)"),
        subtitle = sub
      )
  } else {
    plt <- plt +
      ggtitle(paste0("PCA of phosphoproteomics data ( ", nrow(mat), " variable sites)"))
  }

  return(plt)
}
```

Describe the samples prior to the PCA plotting:

```{r annotations}

annotation <- data.frame(
  Treatment = rep(c(
    rep("Control", 5), rep("Phenytoin_low", 5),
    rep("Phenytoin_high", 5), rep("PTU_low", 5),
    rep("PTU_high", 5)
  ), 3),
  Timepoint = rep(c("2 weeks", "4 weeks", "6 weeks"), each = 25)
)
```

## Variables

```{r setup-variables}
recompute <- 0
nTop <- 100
```

## Conflict resolution

As usual, there are several packages that provide functions with identical
names. Since we are mainly interested in a function of a particular package, we
will clarify this conflict right at the beginning.

**Please note** that functions with the same name from other packages must be
called with their package name as namespace, for example
```AnnotationDbi::select()```.

```{r conflicted}
select <- dplyr::select
filter <- dplyr::filter
rename <- dplyr::rename
group_by <- dplyr::group_by
```

# Load sample table

<p align="right"><a href="#top">&uarr; Back to top</a></p>

We need the samples information alongside other meta data for later purposes
like the sva batch effect detection.

```{r load-sampleTable, results='hide', warning=FALSE, message=FALSE}
filename <- file.path(indir, "XomeTox_RNAseq_Samplesheet.csv",
  fsep = .Platform$file.sep
)

sampleTable <- read_delim(file = filename, delim = ",")
sampleTable$Treatment_group <- as.factor(sampleTable$Treatment_group)

animals_to_use <- unique(sampleTable$Animal_number)
```



# Liver samples


<p align="right"><a href="#top">&uarr; Back to top</a></p>



## Load data files


Load the data table as csv file with all phosphoproteomics samples.

```{r load-liver-data, results='hide', warning=FALSE, message=FALSE}
filename <- file.path(indir, "xometox_phosphoproteomics_ppsites_intensity.csv",
  fsep = .Platform$file.sep
)
data <- read_delim(file = filename, delim = ",")
colnames(data)[1] <- "Site"
```

The table has the following format: Rows describe detected phosphorylation sites, while columns provide the measured intensities.
The first column, however, provides some metainformation about the specific phosphorylation site.


```{r table-format-liver, echo = FALSE}
data[1:5, 1:5]
```

We will save the metainformation in a separate tibble with a special identifier
consisting of the Uniprot_ID, the phosphorylated amino acid and its position.

```{r save-metainformation}
metainfo <- data.frame(str_split_fixed(gsub(" ", "", data$Site), "\\|", 5)) %>%
  mutate(X5 = gsub("\\|", "", X5)) %>%
  mutate(Site = paste0(X1, "_", X3, "_", X4)) %>%
  as_tibble()

colnames(metainfo)[1:5] <- c("Uniprot", "Gene_name", "Amino_acid", "Position", "Surrounding")
```


Now we can exchange the metainformation in the phospho data frame with the
shortend `Site` ID and start working on the phosphoproteomics data.

```{r exchange-site-id}
data$Site <- metainfo$Site
```


## Missing samples

Unfortunately, there are exactly five missing samples within the
phosphoproteomics data. The data.frame has a size of ```r nrow( data)``` rows
and ```r ncol( data) - 1 ``` columns. As stated earlier, rows reflect
phosphorylation sites and column reflect samples.

The missing samples are all recovery related and with exactly one missing sample per recovery treatment group:

* Control_recovery_BR5  - L-A110-G11
* Phenytoin_low_recovery_BR4 - L-A117-G12
* Phenytoin_high_recovery_BR3 - L-A124-G13
* PTU_low_recovery_BR2 - L-A135-G14
* PTU_high_recovery_BR1 - L-A142-G15

## Principal component analysis

To actually get an overview of the samples, we will plot the PCA of the RPM
values.


```{r plot-liver-pca-top100, fig.align='center', fig.cap='PCA of the unnormalized phosphoproteomics data that hase been measured in the liver samples. Colors reflect the treatments while shapes denote different time points. Top100 variable phosphorylation sites have been used to calculate the PCA.', warning=FALSE}
mat <- as.matrix(na.omit(data[, -1]))
rv <- matrixStats::rowVars(mat)
select <- order(rv, decreasing = TRUE)[seq_len(min(nTop, length(rv)))]

plt <- plot_pca(mat[select, ], sub = "Unnormalized phosphorylation sites")
plt
```

The PCA is unfortunately not as conclusive as expected based on the top 100
variable sites.


## Variance stabilizing transformation

As a next step we filter the data set to contain those phosphorylation sites that are
present in one group in at least 3 samples. This is directly followed by the
variance stabilizing transformation. The method ```normalize_vsn()``` from the
```DEP``` package is taken. The VST normalization is done analogously to the
proteomics preprocessing.

```{r filter-liver-reliable-observations, message=FALSE, warning=FALSE}
## filter phosphosites with at least 3 measurements in one treatment group
# data[data == 0] <- NA
rel_filtered <- run_reliable(data, ".*_BR", 3)

## run the variance stabilization
data_vsn <- run_vsn(rel_filtered)
```



Do the same PCA for the variance stabilized RPM values.

```{r plot-liver-pca-vsn, fig.align='center', fig.cap='PCA of the variance stabilized phosphoproteomics data that has been measured in the liver samples. Colors reflect the treatments while shapes denote different time points', warning=FALSE}
mat <- as.matrix(na.omit(data_vsn[, -1]))

plt <- plot_pca(mat, sub = "Variance stabilized data")
plt
```


It seems as if the additional variance stabilization didn't yield any positive
effect. Will this change, when we use only the Top100 variable phosphotylation
sites?

```{r plot-liver-pca-vsn-top100, fig.align='center', fig.cap='PCA of the variance stabilized phosphoproteomics data measured in the liver samples. Colors reflect the treatments while shapes denote different time points. Top100 variable phosphorylation sites have been used to calculate the PCA.', warning=FALSE}
rv <- matrixStats::rowVars(mat)
select <- order(rv, decreasing = TRUE)[seq_len(min(nTop, length(rv)))]

plt <- plot_pca(mat[select, ], sub = "Variance stabilized data")
plt
```

It seems, as it doesn't matter to the PCA at all. Neither with the
IRS-normalized intensities using all phosphorylation sites or simply the Top100
most variable ones, nor with the variance stabilized phosphoproteomics data have
we been able to plot a PCA which enables us to differentiate between different
treatment groups.


## Save data sets

We will save the phosphoproteomics data in their original form and in their
variance stabilized form.

```{r save-liver-data}
filename <- file.path(RdataDir, "01_liver_phosphoproteomics.rda",
  fsep = .Platform$file.sep
)

liver_phosphoproteomics <- data

save(file = filename, liver_phosphoproteomics)
```

```{r save-liver-data_vsn}
filename <- file.path(RdataDir, "01_liver_phosphoproteomics_vsn.rda",
  fsep = .Platform$file.sep
)

liver_phosphoproteomics_vsn <- data_vsn

save(file = filename, liver_phosphoproteomics_vsn)
```



## Finding sources of unwanted variation - `sva`

In the last part, we will try to identiy unwanted source of variation which we
might be able to correct for to improve our PCA-based clustering.


### Load sample information

We need to do some sample ID mapping first...

```{r load-sample-information}
filename <- file.path(indir, "xometox_phosphoproteomics_experiment.xlsx",
  fsep = .Platform$file.sep
)
mapping <- readxl::read_xlsx(
  path = filename, range = "B5:P13",
  col_names = paste0("G", 1:15)
) %>%
  filter(G1 %in% c("2", "4", "5", "6", "8"))

samples <- tibble(
  Sample = colnames(data)[-1],
  Treatment = gsub("_.*", "", colnames(data)[-1])
) %>%
  mutate(Concentration = ifelse(grepl("Control", Sample), NA, ifelse(grepl("low", Sample), "low", "high"))) %>%
  mutate(Timepoint = ifelse(grepl("short", Sample), "2 Weeks", ifelse(grepl("long", Sample), "4 Weeks", "6 Weeks")))
```

Before we can combine the animal mapping with the sample information, we need to
exclude some samples since they were not measured.

```{r thyroid-combine-sample-animal-information}
animals <- mapping %>%
  gather(value = "Animal", key = "Group") %>%
  filter(!Animal %in% c(110, 117, 124, 135, 142))

samples$Animal <- animals %>% pull(Animal)
samples$Treatment_group <- animals %>% pull(Group)

samples$ID <- paste0("L.A", samples$Animal, ".", samples$Treatment_group)
```

### Apply `sva`-package

We will again apply the `sva` package to derive the surrogate variables
that capture the sources of unwanted variation.

Therefore, we will estimate the number of surrogate variables that we should
address with the `num.sv()` function.

Once we have this estimate, we can finally run the identification of hidden
variance by means of the `sva()` method.

```{r sva-filename-1, echo = FALSE}
filename <- file.path(RdataDir, "02_liver_phosphoproteomics_sva.rda",
  fsep = .Platform$file.sep
)

if (!recompute && file.exists(filename)) {
  load(file = filename)
}
```

To run the `sva` method, we specify the model matrix to account for the
different treatment groups.

**Please note** that we need to subset our data set to those 496 sites that have
no `NA` in any of the samples. We will replace the `0` entries with `NA` and
filter them out in order to run the `sva` batch effect identification.

Otherwise, we had to do data imputation to get rid of those data points with no
valuable information (as we did it in the proteomics data set).

```{r run-sva-liver}
#| eval = recompute || ! file.exists( file = filename)

# create the matrix object
# Before running sva, we need to eliminate all 0 or NA values
data[data == 0] <- NA
tib_subset <- as_tibble(data) %>% filter(across(c(-Site), ~ !is.na(.x)))
mat <- as.matrix(tib_subset[, -1])

## Prepare the sva run
samples_sub <- samples[match(colnames(data)[-1], samples$Sample), ]
mod <- model.matrix(~Treatment_group, data = samples_sub)
mod0 <- model.matrix(~1, data = samples_sub)

## estimate the number of surrogate variables
n.sv <- num.sv(mat, mod, method = "leek")

svobj <- sva(mat, mod, mod0, n.sv = n.sv)

save(svobj, mat, mod, mod0, samples_sub, tib_subset, file = filename)
```

To check if the method succeeded in capturing the variance that caused the PCA
in any of the surrogate variables, we will plot the PCA with colors according to
the `sva` results.

**Please note** this PCA contains only the subset of nearly 500 phosphorylation
sites that have been measured in all samples. The data used are the unnormalized
intensities.

```{r plot-pca-sva-liver, fig.align='center', fig.cap='PCA of the imputed data measured in the liver samples. The Top50 variable phosphorylation sites were used for this PCA. Colors reflect the surrogate variable of factor 1 as it was determined by sva in the previous step.', warning=FALSE}
## plot the pca with sva-based coloring
## again use the top 500 variable proteins (same list as before)
pc <- prcomp(t(mat))
percentVar <- round(pc$sdev^2 / sum(pc$sdev^2) * 100, digits = 2)
pca_data <- data.frame(samples = rownames(pc$x), PC1 = pc$x[, 1], PC2 = pc$x[, 2])

pca_data$sv1 <- svobj$sv[, 1]


plt <- ggplot(pca_data, aes(x = PC1, y = PC2, color = sv1)) +
  geom_point(size = 3) +
  xlab(paste0("PC1: ", percentVar[1], "% variance")) +
  ylab(paste0("PC2: ", percentVar[2], "% variance")) +
  ggtitle(paste0("PCA of proteomics data ( ", nrow(mat), " variable proteins)")) +
  theme_bw()

plt
```


The `sva` package was able to identify a single surrogate variable that captures
mainly the variance that also drives PC1 in our previous principal component
analysis. 

In a next step, we might now want to remove this 'effect' from our data. By
doing so we might be able to discriminate more effectively between our different
treatment groups.

## Remove batch effect

It seems that surrogate variable 1 precisely covers the variance that is
responsible for the PC1. Similar to the thyroid transcriptome data,
we can make use of the `limma` package to remove this batch effect and get a
nicer PCA.

```{r file-removed-batch-effect}
filename <- file.path(RdataDir, "03_liver_phosphoproteomics_corrected.rda",
  fsep = .Platform$file.sep
)

if (!recompute && file.exists(filename)) {
  load(file = filename)
}
```

```{r remove-batch-effect}
#| eval = recompute || ! file.exists( file = filename)

mat_corrected <- removeBatchEffect(mat, covariates = svobj$sv[, 1], design = mod)
```

Let's save the corrected phosphoproteomics data in a tibble format where the
first column specifies the phosphorylation site and the remaining columns
represent the samples.

```{r save-corrected-data}
#| eval = recompute || ! file.exists( file = filename)

# corrected dataframe is needed due to the sample name mapping in the colnames
# corrected object needs to preserve the original names (Control_short_BR1) until the vst
corrected <- bind_cols(tib_subset[, 1], as_tibble(mat_corrected))
liver_phosphoproteomics_corrected <- corrected
colnames(liver_phosphoproteomics_corrected)[-1] <- samples$ID[match(colnames(liver_phosphoproteomics_corrected)[-1], samples$Sample)]

save(liver_phosphoproteomics_corrected, file = filename)
```



**Variance stabilizing transformation**

After correcting for batch effects, we will again run a variance stabilizing
transformation on that data set.

```{r filename-liver-corrected-vsn}
filename <- file.path(RdataDir, "04_liver_phosphoproteomics_corrected_vsn.rda",
  fsep = .Platform$file.sep
)

if (!recompute && file.exists(filename)) {
  load(file = filename)
}
```

```{r save-liver-corrected-vsn, results='hide', message=FALSE, warning=FALSE}
#| eval = recompute || !file.exists( filename)

## run the variance stabilization
corrected_vsn <- as_tibble(run_vsn(corrected))
liver_phosphoproteomics_corrected_vsn <- corrected_vsn
colnames(liver_phosphoproteomics_corrected_vsn)[-1] <- samples$ID[match(colnames(liver_phosphoproteomics_corrected_vsn)[-1], samples$Sample)]

save(liver_phosphoproteomics_corrected_vsn, file = filename)
```

```{r filename-liver-corrected-vsn-se}
filename <- file.path(RdataDir, "04_liver_phosphoproteomics_corrected_vsn_se.rda",
  fsep = .Platform$file.sep
)

if (!recompute && file.exists(filename)) {
  load(file = filename)
}
```

```{r save-liver-corrected-vsn-se, results='hide', message=FALSE, warning=FALSE}
#| eval = recompute || !file.exists( filename)

## run the variance stabilization
liver_phosphoproteomics_corrected_vsn_se <- create_se(liver_phosphoproteomics_corrected_vsn)

## change the ID and condition
animal <- gsub(".*A([0-9]+).G.*", "\\1", colData(liver_phosphoproteomics_corrected_vsn_se)$label)
group <- gsub(".*G([0-9]+)", "\\1", colData(liver_phosphoproteomics_corrected_vsn_se)$label)

colData(liver_phosphoproteomics_corrected_vsn_se)$ID <- paste0("X",group,"_",animal)
colData(liver_phosphoproteomics_corrected_vsn_se)$condition <- paste0("X",group)
colData(liver_phosphoproteomics_corrected_vsn_se)$replicate <- animal

save(liver_phosphoproteomics_corrected_vsn_se, file = filename)
```

## Create the `MOFA` input data

To combine multiple omics layers before actually training the `MOFA` object, we
need to create a long data.frame from the batch corrected count data.

```{r reshape-phosphoproteomics}
filename <- file.path(RdataDir, "06_liver_phosphoproteomics_input.rda",
  fsep = .Platform$file.sep
)

if (recompute || !file.exists(filename)) {
  phospho <- liver_phosphoproteomics_corrected_vsn %>%
    gather(key = "sample", value = "value", -Site) %>%
    mutate(group = gsub(".*.G", "", sample)) %>%
    mutate(view = "Phosphoproteomics") %>%
    dplyr::rename("feature" = Site) %>%
    select(feature, view, sample, value, group) %>%
    as.data.frame()

  save(phospho, file = filename)

  rm(liver_phosphoproteomics_corrected_vsn)
  
} else {
  
  load(file = filename)
  
}
```


# R Session

```{r session}
sessionInfo()
```












