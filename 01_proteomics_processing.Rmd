---
title: "Preprocessing proteomics data"
author: "Sebastian Canzler"
date: "June 13th, 2024"
output:
  bookdown::html_document2:
    toc: true
    toc_float: true
    number_sections: true
    theme: united 
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```


# Aim

<p align="right"><a href="#top">&uarr; Back to top</a></p>

This Rmarkdown file covers the processing of proteomics data in liver 
and thyroid samples of the XomeTox Project.

Starting from count data in a given directory, we will mean normalize the TMT
batches, transform the data using the variance-stablizing transformation and
filter the data set according to the MOFA2 tutorial.


# Reproducibility

To reproduce the analysis please use the following singularity container: `rocker_multiomics_analysis:latest.sif`

You can download the container from docker hub: `docker://boll3/rocker_multiomics:latest`

Digest: `db12fdf`

This `.Rmd` file that is the basis of this report has been rendered via:

```{r render, eval=FALSE}
rmarkdown::render("01_proteomics_processing.Rmd", output_format = "html_document")
```


# Set-up

<p align="right"><a href="#top">&uarr; Back to top</a></p>

Here, all directories are set to the correct paths of this project. Depending on
the machine that is used, the number of CPUs are adjusted.

```{r set-directories, echo = FALSE}
#####################################
# CHANGE WORKING DIRECTORY HERE !!! #
#####################################
my_wd <- "/path/to/workingdir/"
my_wd <- "/home/canzler/git-canzler/projects/2022_multi_omics_paper_scripts"
setwd(my_wd)

data_dir <- "/home/canzler/git-canzler/projects/2022_multi_omics_paper_scripts/DATA/proteomics/01_preprocessing"
####################################
# CHANGE OUTPUT DIRECTORY HERE !!! #
####################################
# for storing .Rdata files
RdataDir <- file.path("/path/to/directory", "RData", fsep = .Platform$file.sep)
RdataDir <- file.path(data_dir, "RData", fsep = .Platform$file.sep)
if (!dir.exists(RdataDir)) {
  dir.create(RdataDir)
}


###################################
# CHANGE INPUT DIRECTORY HERE !!! #
###################################
# for storing input files
indir <- file.path("/path/to/directory", "input", fsep = .Platform$file.sep)
indir <- file.path(data_dir, "input", fsep = .Platform$file.sep)
if (!dir.exists(indir)) {
  dir.create(indir)
}
```

* Working directory: ```r my_wd```
* Data directory on EVE: ```r data_dir```
* RData storage: ```r RdataDir```


```{r load-libraries-2, warning=FALSE, results='hide', message=FALSE}
library("DEP")
library("sva")
library("limma")
library("knitr")
library("readxl")
library("xtable")
library("tidyverse")
library("magrittr")
library("DT")
library("SummarizedExperiment")
library("gtools")
```


## Functions and annotations

```{r functions}
#' Function for row-wise mean normalization
#'
#' This function calculates the mean for each row of a given data.frame or
#' tibble and uses this as a normalization factor.
#'
#' @param df Data.frame or tibble that must only contain numeric columns
#'
#' @return Data.frame of the same size where each value is normalized by
#'   its row mean
#'
#' @export
mean_normalization <- function(df) {
  return(df / rowMeans(df, na.rm = TRUE))
}


#' Create a summarized Experiment object from data table
#'
#' This method re-formats a given data.frame with intensities into a summarized
#' experiment object by means of the DEP package.
#'
#' @param df Data.frame with measured intensities. The first column represents
#'   feature IDs while the remaining columns represent samples. Rows are
#'   measured features.
#'
#' @return SummarizedExperiment object.
#'
#' @importFrom DEP make_unique make_se
create_se <- function(df) {
  ## get colnames of given data frame
  label <- colnames(df)[-1]

  ## get experimental design
  exp_design <- data.frame(label,
    condition = gsub("_BR[0-9]+", "", label),
    replicate = gsub(".*_BR", "BR", label)
  )

  ## set 'name' and 'ID' column
  df_unique <- make_unique(df,
    names = "Accession",
    ids = "Accession",
    delim = ";"
  )

  ## create a summarized experiment from data frame
  se <- make_se(
    proteins_unique = df_unique,
    columns = 2:ncol(df),
    expdesign = exp_design
  )

  return(se)
}


#' Run variance-stabilizing normalization on a given data.frame
#'
#' This method re-formats a given data.frame with intensities into a summarized
#' experiment object by means of the DEP package.
#' A variance stabilizing normalization is subsequently applied and the
#' corresponding data.frame is returned
#'
#' @param df Data.frame with measured intensities. The first column represents
#'   feature IDs while the remaining columns represent samples. Rows are
#'   measured features.
#'
#' @return Data.frame with variance stabilized intensities.
#'
#' @importFrom DEP normalize_vsn get_df_wide
#' @importFrom dplyr select
run_vsn <- function(df) {
  se <- create_se(df)

  ## apply variance stabilized normalization
  norm <- normalize_vsn(se)

  ## Remove columns 'name' and 'ID' and rechange the order to put 'Accession' at first position
  df_norm <- get_df_wide(norm) %>%
    select(-c("name", "ID")) %>%
    select(Accession, everything())

  return(df_norm)
}


reliable_identification <- function(data, reliable) {
  ## calculate the number of observations for each row
  ## filter data based on those rows where we have more
  ## than reliable observations
  prots <- data %>%
    rowwise() %>%
    dplyr::mutate(re = sum(!is.na(c_across(-Accession)))) %>%
    dplyr::filter(re >= reliable) %>%
    dplyr::pull(Accession)

  # you only need to return the accession IDs
  return(prots)
}


#' Filter a data set for features based on the number of measurements in each
#' contrast group
#'
#' Filtering procedure is done as follows: Check for each treatment group that
#' the filtered features are measured in more samples than specified in the
#' variable 'observations'. Make a unique list of all features from all
#' treatment groups and subset the given data frame to those features.
#'
#' @param data Data.frame with the data to be filtered. Columns are samples,
#'   rows are features. Accession column represents the feature name.
#' @param replicate String or pattern that is needed to identify the individual
#'   treatment groups.
#' @param observations Integer speficifying the number of samples where a
#'   certain feature has to be measured.
#'
#' @return  Data.frame with the subsetted set of features
#'
#' @importFrom dplyr filter
#'
#' @export
run_reliable <- function(data, replicate, observations) {
  ## get all samples
  samples <- unique(gsub(replicate, "", colnames(data)[-1]))

  ## get list of proteins with number of observations > reliable
  ## per contrast
  feature_list <- lapply(samples, function(sample) {
    columns <- colnames(data)[grep(paste0("Accession|", sample), colnames(data))]
    reliable_identification(data[, columns], observations)
  })

  # make unique list of proteins
  features <- unique(unlist(feature_list))

  ## subset the original data to those proteins
  return(data %>% filter(Accession %in% features))
}

#' Function to plot a PCA
#'
#' Customized color scheme for XomeTox samples
#'
#' @param mat Matrix object to calculate the PCA from
#'
#' @return ggplot object
plot_pca <- function(mat) {
  pc <- prcomp(t(mat))
  percentVar <- round(pc$sdev^2 / sum(pc$sdev^2) * 100, digits = 2)
  pca_data <- data.frame(samples = rownames(pc$x), PC1 = pc$x[, 1], PC2 = pc$x[, 2])
  pca_data$Treatment <- gsub("_(short|long|recovery)_BR.*", "", pca_data$samples)
  pca_data$Timepoint <- ifelse(grepl("short", pca_data$samples), "2 Weeks", ifelse(grepl("long", pca_data$samples), "4 Weeks", "6 Weeks"))

  plt <- ggplot(pca_data, aes(x = PC1, y = PC2, color = Treatment, shape = Timepoint)) +
    geom_point(size = 3) +
    scale_color_manual(
      breaks = c(
        "Control", "Phenytoin_low", "Phenytoin_high",
        "PTU_low", "PTU_high"
      ),
      values = c(
        "orange", "green", "green4",
        "royalblue2", "blue3"
      )
    ) +
    xlab(paste0("PC1: ", percentVar[1], "% variance")) +
    ylab(paste0("PC2: ", percentVar[2], "% variance")) +
    ggtitle(paste0("PCA of proteomics data ( ", nrow(mat), " variable proteins)")) +
    theme_bw()

  return(plt)
}
```

Describe the samples prior to the PCA plotting

```{r annotations}

annotation <- data.frame(
  Treatment = rep(c(
    rep("Control", 5), rep("Phenytoin_low", 5),
    rep("Phenytoin_high", 5), rep("PTU_low", 5),
    rep("PTU_high", 5)
  ), 3),
  Timepoint = rep(c("2 weeks", "4 weeks", "6 weeks"), each = 25)
)
```

## Variables

```{r setup-variables}
recompute <- 0
nTop <- 500
```

## Conflict resolution

As usual, there are several packages that provide functions with identical
names. Since we are mainly interested in a function of a particular package, we
will clarify this conflict right at the beginning.

**Please note** that functions with the same name from other packages must be
called with their package name as namespace, for example
`AnnotationDbi::select()`.

```{r conflicted}
select <- dplyr::select
filter <- dplyr::filter
rename <- dplyr::rename
group_by <- dplyr::group_by
```

# Load sample table

<p align="right"><a href="#top">&uarr; Back to top</a></p>

We need the samples information alongside other meta data for later purposes
like the sva batch effect detection.

```{r load-sampleTable, results='hide', message=FALSE, warning=FALSE}
filename <- file.path(indir, "XomeTox_RNAseq_Samplesheet.csv",
  fsep = .Platform$file.sep
)

sampleTable <- read_delim(file = filename, delim = ",")
sampleTable$Treatment_group <- as.factor(sampleTable$Treatment_group)

animals_to_use <- unique(sampleTable$Animal_number)
```



# Thyroid samples

<p align="right"><a href="#top">&uarr; Back to top</a></p>

## Load data files

Load the data table as csv file with all identified proteins - MasterProteins
and all other things like contaminants - and the abundance normalized reporter
intensities.

```{r load-thyroid-data, results='hide', message=FALSE, warning=FALSE}
filename <- file.path(indir, "XomeTox_S_tmt_proteome_set_Proteins.txt",
  fsep = .Platform$file.sep
)

data <- read_delim(file = filename, delim = "\t", quote = '"')
```

No we need the mapping of reporter and labels to the actual samples.

Sample mapping for TMT batch 2 are unfortunately missing in this mapping, but
the normalized intensities are present in the data files. We therefore need to
add them to this mapping.

```{r load-thyroid-samples, results='hide', message=FALSE, warning=FALSE}
filename <- file.path(indir, "XomeTox_S_tmt_proteome_set_Proteins_experiment.xlsx",
  fsep = .Platform$file.sep
)

samples <- readxl::read_xlsx(
  path = filename, skip = 4,
  col_names = c("Reporter", "Sample", "Color")
) %>%
  filter(!grepl("IRS_BR", Sample))

reporter_new <- c(
  "F2 127N", "F2 127C", "F2 128N", "F2 128C", "F2 129N",
  "F2 129C", "F2 130N", "F2 131N", "F2 131C", "F2 132N",
  "F2 132C", "F2 133N", "F2 133C", "F2 134N"
)
reporter_new <- paste0("Abundances Normalized ", reporter_new, " Sample")
sample_new <- c(
  "Control_short_BR2", "Phenytoin_low_short_BR2", "Phenytoin_high_short_BR2",
  "PTU_low_short_BR2", "PTU_high_short_BR2",
  "Control_long_BR2", "Phenytoin_low_long_BR2",
  "PTU_low_long_BR2", "PTU_high_long_BR2",
  "Control_recovery_BR2", "Phenytoin_low_recovery_BR2", "Phenytoin_high_recovery_BR2",
  "PTU_low_recovery_BR2", "PTU_high_recovery_BR2"
)
color_new <- c(
  "lightgray", "cyan", "springgreen", "violet", "peachpuff",
  "darkgray", "darkcyan", "darkviolet", "chocolate", "gray",
  "darkslategray", "green", "blueviolet", "sienna"
)

samples <- bind_rows(
  samples,
  tibble(
    Reporter = reporter_new,
    Sample = sample_new,
    Color = color_new
  )
) %>%
  mutate(Treatment = gsub("_.*", "", Sample)) %>%
  mutate(Concentration = ifelse(grepl("Control", Sample), NA, ifelse(grepl("low", Sample), "low", "high"))) %>%
  mutate(Timepoint = ifelse(grepl("short", Sample), "2 Weeks", ifelse(grepl("long", Sample), "4 Weeks", "6 Weeks")))
```

Define the mapping of group names to specific treatment groups:

```{r map-colnames-to-treatmentgroups}
samples$Treatment_group <- "1"
samples[grepl("Phenytoin_low_short", samples$Sample), ] %<>% mutate(Treatment_group = "2")
samples[grepl("Phenytoin_high_short", samples$Sample), ] %<>% mutate(Treatment_group = "3")
samples[grepl("PTU_low_short", samples$Sample), ] %<>% mutate(Treatment_group = "4")
samples[grepl("PTU_high_short", samples$Sample), ] %<>% mutate(Treatment_group = "5")
samples[grepl("Control_long", samples$Sample), ] %<>% mutate(Treatment_group = "6")
samples[grepl("Phenytoin_low_long", samples$Sample), ] %<>% mutate(Treatment_group = "7")
samples[grepl("Phenytoin_high_long", samples$Sample), ] %<>% mutate(Treatment_group = "8")
samples[grepl("PTU_low_long", samples$Sample), ] %<>% mutate(Treatment_group = "9")
samples[grepl("PTU_high_long", samples$Sample), ] %<>% mutate(Treatment_group = "10")
samples[grepl("Control_recovery", samples$Sample), ] %<>% mutate(Treatment_group = "11")
samples[grepl("Phenytoin_low_recovery", samples$Sample), ] %<>% mutate(Treatment_group = "12")
samples[grepl("Phenytoin_high_recovery", samples$Sample), ] %<>% mutate(Treatment_group = "13")
samples[grepl("PTU_low_recovery", samples$Sample), ] %<>% mutate(Treatment_group = "14")
samples[grepl("PTU_high_recovery", samples$Sample), ] %<>% mutate(Treatment_group = "15")
```

Now add the Animal number to the equation:

```{r derive-animal-number}
samples <- samples %>%
  mutate(Replicate = gsub(".*BR", "", Sample)) %>%
  mutate(Animal = as.integer(Replicate) + (as.integer(Treatment_group) - 1) * 10) %>%
  mutate(SampleID = paste0("S.A", Animal, ".G", Treatment_group))
```


Let's now run some sanity checks to make sure that the mapping information is correct:

* **Number of unique samples (should be 149):** ```r length( unique(samples$Reporter))```
* **Number of unique treatment groups (15):** ```r length( unique( gsub( "_BR.*", "", samples$Sample)))```
* **Number of replicates per treatment group:** 

```{r replicates-per-treatment-thyroid, results='asis', echo=FALSE}
table(gsub("_BR.*", "", samples$Sample))
```

Normally, we would have exactly ten biological replicates per treatment group,
but due to the missing sample S-A72-G8, we should only have nine in the group
'Phenytoin_high_long".

## Process thyroid data

In the first steps, we subset the proteomics data to those columns and rows that
are really needed. Necessary columns are: ```Accession``` and all ```r length(
unique(samples$Reporter))``` Reporter columns that have a mapping in the mapping
table. Rows that are needed: those that are MasterProteins and those that have
the Term "rat" in their description. Only the first Accession term will be used
as protein accession id.

```{r filter-thyroid-data}
columns_to_keep <- colnames(data)[colnames(data) %in% samples$Reporter]

filtered_data <- data %>%
  filter(Master == "IsMasterProtein") %>%
  filter(grepl("Rattus", Description, ignore.case = TRUE)) %>%
  select(c(Accession, all_of(columns_to_keep))) %>%
  rowwise() %>%
  dplyr::mutate(nas = sum(is.na(c_across(-c(Accession))))) %>%
  filter(nas != 149) %>%
  ungroup() %>%
  select(-nas)
```


## Mean normalization

After the previous filtering we can now apply the mean normalization step.
We therefore need to split the data into the single TMT batches based on their
"F[1-9]0?" reporter description. The normalization is actually done for each
protein separately on this reduced data set.

```{r thyroid-mean-normalization}
# define the set of TMT batches that are needed to split the data frame
batches <- paste0("F", seq(1:10), " ")

# run over each batch to calculate the mean normalization
list <- lapply(batches, function(batch) {
  mean_normalization(filtered_data[, grep(batch, colnames(filtered_data))])
})

# Combine the data sets back together
normdata <- cbind(
  filtered_data$Accession,
  do.call(cbind, list)
)

colnames(normdata)[1] <- "Accession"
m <- match(colnames(normdata)[-1], samples$Reporter)
colnames(normdata)[-1] <- samples$Sample[m]
```

## Variance stabilizing transformation

As a next step we filter the data set to contain those proteins that are present
in one group in at least 7 samples. This is directly followed by the variance
stabilizing transformation. The method ```normalize_vsn()``` from the ```DEP```
package is taken. 

```{r filter-thyroid-reliable-observations, message=FALSE, warning=FALSE}
## filter proteins with at least 7 measurements in one treatment group
rel_filtered <- run_reliable(normdata, "_BR[0-9]0?", 7)

## run the variance stabilization
mean_normdata <- run_vsn(rel_filtered)
```

## Principal component analysis

We will plot the PCA of the mean normalized and variance-stabilized data.

```{r plot-thyroid-pca-vsn-top500, fig.align='center', fig.cap='PCA of the mean normalized data based on the top 500 variable proteins. Colors reflect the treatments while shapes denote different time points', warning=FALSE}
mat <- as.matrix(na.omit(mean_normdata[, -1]))
rv <- rowVars(mat)
sel <- order(rv, decreasing = TRUE)[seq_len(min(nTop, length(rv)))]

plot_pca(mat[sel, ])
```

## Save data

Save the variance stabilized data set as input for the subsequent multi-omics
data integration. We will save the full data set as well a subsetted one that
only contains those samples that are also measured in transcriptomics.

```{r thyroid-save-data}
filename <- file.path(RdataDir, "01_thyroid_proteomics_vsn_full.rda",
  fsep = .Platform$file.sep
)

m <- match(colnames(mean_normdata)[-1], samples$Sample)
thyroid_proteomics_vsn_full <- as_tibble(mean_normdata)
colnames(thyroid_proteomics_vsn_full)[-1] <- samples$SampleID[m]

save(thyroid_proteomics_vsn_full, file = filename)
```

To calculate the subsetted vsn transformed data set, we need to excluded all
unwanted columns from the data. Afterwards we filter for proteins measured in at
least three of five samples per treatment group before we transform the data.

```{r thyroid-subset-original-data}
samples_to_keep <- samples %>%
  filter(Animal %in% animals_to_use) %>%
  pull(Sample)

normdata_sub <- normdata %>%
  select(c(Accession, all_of(samples_to_keep)))
```

```{r thyroid-run-vsn-subset}
## filter proteins with at least 3 measurements in one treatment group
rel_filtered_sub <- run_reliable(normdata_sub, "_BR[0-9]0?", 3)

## run the variance stabilization
mean_normdata_sub <- run_vsn(rel_filtered_sub)

## save also the summarizedExperiment as a whole object
## for subsequent differential expression analysis

thyroid_proteomics_se <- create_se(rel_filtered_sub)
thyroid_proteomics_vsn_se <- normalize_vsn(thyroid_proteomics_se)
```

```{r thyroid-save-data-subset}
filename <- file.path(RdataDir, "01_thyroid_proteomics_vsn.rda",
  fsep = .Platform$file.sep
)

m <- match(colnames(mean_normdata_sub)[-1], samples$Sample)
thyroid_proteomics_vsn <- as_tibble(mean_normdata_sub)
colnames(thyroid_proteomics_vsn)[-1] <- samples$SampleID[m]

save(thyroid_proteomics_vsn, file = filename)


filename <- file.path(RdataDir, "01_thyroid_proteomics_vsn_se.rda",
  fsep = .Platform$file.sep
)

m <- match(colnames(thyroid_proteomics_vsn_se), samples$Sample)
colnames(thyroid_proteomics_vsn_se) <- samples$SampleID[m]

save(thyroid_proteomics_vsn_se, file = filename)
```

## Create `MOFA2` object

In a last step, we combine the data into a long data.frame that is suitable for
multi-omics data integration with `MOFA2`.


```{r reshape-tissue-proteomics}
filename <- file.path(RdataDir, "04_thyroid_proteomics_input.rda",
  fsep = .Platform$file.sep
)

if (recompute || !file.exists(filename)) {
  prot <- thyroid_proteomics_vsn %>%
    gather(key = "sample", value = "value", -Accession) %>%
    mutate(group = gsub(".*.G", "", sample)) %>%
    mutate(view = "Proteomics") %>%
    dplyr::rename("feature" = Accession) %>%
    select(feature, view, sample, value, group) %>%
    as.data.frame()

  save(prot, file = filename)
} else {
  load(file = filename)
}

rm(thyroid_proteomics_vsn)
```



# Liver samples


<p align="right"><a href="#top">&uarr; Back to top</a></p>

## Load data files

Load the data table as csv file with all identified proteins - MasterProteins
and all other things like contaminants - and the abundance normalized reporter
intensities.

```{r load-liver-data, results='hide', warning=FALSE, message=FALSE}
filename <- file.path(indir, "XomeTox_L_tmt_proteome_set_Proteins.txt",
  fsep = .Platform$file.sep
)

data <- read_delim(file = filename, delim = "\t", quote = '"')
```


No we need the mapping of reporter and labels to the actual samples.

Sample mapping for TMT batch 2 are unfortunately missing in this mapping, but
the normalized intensities are present in the data files. We therefore need to
add them to this mapping.

```{r load-liver-samples, results='hide', message=FALSE, warning=FALSE}
filename <- file.path(indir, "XomeTox_L_tmt_proteome_set_Proteins_experiment.xlsx",
  fsep = .Platform$file.sep
)

samples <- readxl::read_xlsx(
  path = filename, skip = 4,
  col_names = c("Reporter", "Sample", "Color")
) %>%
  filter(!grepl("IRS_BR", Sample)) %>%
  mutate(Treatment = gsub("_.*", "", Sample)) %>%
  mutate(Concentration = ifelse(grepl("Control", Sample), NA, ifelse(grepl("low", Sample), "low", "high"))) %>%
  mutate(Timepoint = ifelse(grepl("short", Sample), "2 Weeks", ifelse(grepl("long", Sample), "4 Weeks", "6 Weeks")))
```

Define the mapping of group names to specific treatment groups:

```{r liver-map-colnames-to-treatmentgroups}
samples$Treatment_group <- "1"
samples[grepl("Phenytoin_low_short", samples$Sample), ] %<>% mutate(Treatment_group = "2")
samples[grepl("Phenytoin_high_short", samples$Sample), ] %<>% mutate(Treatment_group = "3")
samples[grepl("PTU_low_short", samples$Sample), ] %<>% mutate(Treatment_group = "4")
samples[grepl("PTU_high_short", samples$Sample), ] %<>% mutate(Treatment_group = "5")
samples[grepl("Control_long", samples$Sample), ] %<>% mutate(Treatment_group = "6")
samples[grepl("Phenytoin_low_long", samples$Sample), ] %<>% mutate(Treatment_group = "7")
samples[grepl("Phenytoin_high_long", samples$Sample), ] %<>% mutate(Treatment_group = "8")
samples[grepl("PTU_low_long", samples$Sample), ] %<>% mutate(Treatment_group = "9")
samples[grepl("PTU_high_long", samples$Sample), ] %<>% mutate(Treatment_group = "10")
samples[grepl("Control_recovery", samples$Sample), ] %<>% mutate(Treatment_group = "11")
samples[grepl("Phenytoin_low_recovery", samples$Sample), ] %<>% mutate(Treatment_group = "12")
samples[grepl("Phenytoin_high_recovery", samples$Sample), ] %<>% mutate(Treatment_group = "13")
samples[grepl("PTU_low_recovery", samples$Sample), ] %<>% mutate(Treatment_group = "14")
samples[grepl("PTU_high_recovery", samples$Sample), ] %<>% mutate(Treatment_group = "15")
```

Now add the Animal number to the equation:

```{r derive-animal-number-liver}
samples <- samples %>%
  mutate(Replicate = gsub(".*BR", "", Sample)) %>%
  mutate(Animal = as.integer(Replicate) + (as.integer(Treatment_group) - 1) * 10) %>%
  mutate(SampleID = paste0("L.A", Animal, ".G", Treatment_group))
```


In contrast to the thyroid samples, we do not need to adapt the sample
information. TMT-batch 1 was lost due to errors in the preparation and hence
cannot be used. All remaining 135 reporter values are present in the data as
well as in the sampling information.

**Please note** that the lost TMT-batch was **not** measured at all!

Let's now run some sanity checks to make sure that the mapping information is correct:

* **Number of unique samples (should be 135):** ```r length( unique(samples$Reporter))```
* **Number of unique treatment groups (15):** ```r length( unique( gsub( "_BR.*", "", samples$Sample)))```
* **Number of replicates per treatment group:** 

```{r replicates-per-treatment-liver, results='asis', echo=FALSE}
table(gsub("_BR.*", "", samples$Sample))
```

Normally, we would have exactly ten biological replicates per treatment group,
but due to the missing missing TMT-batch 1, we should only have nine in each
treatment group.


## Process liver data

In the first steps, we subset the proteomics data to those columns and rows that are really needed.

Necessary columns are: ```Accession``` and all ```r length( unique(samples$Reporter))``` Reporter columns that have a mapping in the mapping table.

Rows that are needed: those that are MasterProteins and those that have the Term "Rattus" in their description.

Only the first Accession term will be used as protein accession id.

```{r filter-liver-data}
columns_to_keep <- colnames(data)[colnames(data) %in% samples$Reporter]

filtered_data <- data %>%
  filter(Master == "IsMasterProtein") %>%
  filter(grepl("Rattus", Description, ignore.case = TRUE)) %>%
  select(c(Accession, all_of(columns_to_keep))) %>%
  rowwise() %>%
  dplyr::mutate(nas = sum(is.na(c_across(-c(Accession))))) %>%
  filter(nas != 135) %>%
  ungroup() %>%
  select(-nas)
```


## Mean normalization

After the previous filtering we can now apply the mean normalization step.
We therefore need to split the data into the single TMT batches based on their
"F[1-9]0?" reporter description. The normalization is actually done for each
protein separately on this reduced data set.

```{r liver-mean-normalization}
# define the set of TMT batches that are needed to split the data frame
batches <- paste0("F", seq(1:9), " ")

# run over each batch to calculate the mean normalization
list <- lapply(batches, function(batch) {
  mean_normalization(filtered_data[, grep(batch, colnames(filtered_data))])
})

# Combine the data sets back together
normdata <- cbind(
  filtered_data$Accession,
  do.call(cbind, list)
)
colnames(normdata)[1] <- "Accession"

m <- match(colnames(normdata)[-1], samples$Reporter)
colnames(normdata)[-1] <- samples$Sample[m]
```



## Variance stabilizing transformation

As a next step we filter the data set to contain those proteins that are present
in one group in at least 7 samples. This is directly followed by the variance
stabilizing transformation. The method ```normalize_vsn()``` from the ```DEP```
package is taken. The creation of the summarizedExperiment is done analogously
to how Zhipeng did it for the IRS-normalized data set.

```{r filter-liver-reliable-observations, message=FALSE, warning=FALSE}
## filter proteins with at least 7 measurements in one treatment group
rel_filtered <- run_reliable(normdata, "_BR[0-9]0?", 7)

## run the variance stabilization
mean_normdata <- run_vsn(rel_filtered)
```

## Principal component analysis

To actually evaluate which data set is the best to use for our multi-omics
integration, we will plot the PCA of the mean normalized data and the data that
was additionally variance stabilized.


```{r plot-liver-pca-mean-normalized-top500, fig.align='center', fig.cap='PCA of the mean normalized data measured in the liver samples. Colors reflect the treatments while shapes denote different time points', warning=FALSE}
mat <- as.matrix(na.omit(mean_normdata[, -1]))
rv <- matrixStats::rowVars(mat)
select_ids <- order(rv, decreasing = TRUE)[seq_len(min(nTop, length(rv)))]

plt <- plot_pca(mat[select_ids, ])

plt
```

## Save data

Save the variance stabilized data set as input for the subsequent multi-omics
data integration. We will save the full data set as well a subsetted one that
only contains those samples that are also measured in transcriptomics.

```{r liver-save-data}
filename <- file.path(RdataDir, "01_liver_proteomics_vsn_full.rda",
  fsep = .Platform$file.sep
)

m <- match(colnames(mean_normdata)[-1], samples$Sample)
liver_proteomics_vsn_full <- as_tibble(mean_normdata)
colnames(liver_proteomics_vsn_full)[-1] <- samples$SampleID[m]

save(liver_proteomics_vsn_full, file = filename)
```

To calculate the subsetted vsn transformed data set, we need to excluded all
unwanted columns from the data. Afterwards we filter for proteins measured in at
least three of five samples per treatment group before we transform the data.

```{r liver-subset-original-data}
samples_to_keep <- samples %>%
  filter(Animal %in% animals_to_use) %>%
  pull(Sample)

normdata_sub <- normdata %>%
  select(c(Accession, all_of(samples_to_keep)))
```

```{r liver-run-vsn-subset}
## filter proteins with at least 7 measurements in one treatment group
rel_filtered_sub <- run_reliable(normdata_sub, "_BR[0-9]0?", 3)

## run the variance stabilization
mean_normdata_sub <- run_vsn(rel_filtered_sub)
```

```{r liver-save-data-subset}
filename <- file.path(RdataDir, "01_liver_proteomics_vsn.rda",
  fsep = .Platform$file.sep
)

m <- match(colnames(mean_normdata_sub)[-1], samples$Sample)
liver_proteomics_vsn <- as_tibble(mean_normdata_sub)
colnames(liver_proteomics_vsn)[-1] <- samples$SampleID[m]

save(liver_proteomics_vsn, file = filename)
```



## Finding sources of unwanted variation - `sva`

Before we can run `sva`, we need to impute our data, since `sva` is not able to
cope with missing values.

### Data imputation with `DEP`

```{r file-imputation}
filename <- file.path(RdataDir, "02_liver_proteomics_imputed.rda",
  fsep = .Platform$file.sep
)

if (!recompute && file.exists(filename)) {
  load(file = filename)
}
```

To impute our liver proteomics data, we will utilize the `DEP` package which
provides several methods for this specific task. Therefore, we need to create a
`SummarizedExperiment` object and again run the variance stabilized
transformation on it.

```{r create-data-object}
#| eval = recompute || ! file.exists( file = filename)

# create summarized experiment object
data <- rel_filtered_sub
m <- match(colnames(data)[-1], samples$Sample)
colnames(data)[-1] <- samples$SampleID[m]

## get experimental design
exp_design <- data.frame(
  label = samples$SampleID[m],
  condition = samples$Treatment_group[m],
  replicate = samples$Replicate[m]
)


## set 'name' and 'ID' column
data_unique <- make_unique(data,
  names = "Accession",
  ids = "Accession",
  delim = ";"
)

## create a summarized experiment from data frame
se <- make_se(
  proteins_unique = data_unique,
  columns = 2:ncol(data),
  expdesign = exp_design
)

vsn <- normalize_vsn(se)
```


In the following, we will try three different imputation approaches on our
proteomics data: 

```{r run-imputation}
#| eval = recompute || ! file.exists( file = filename)

# Impute missing data using the k-nearest neighbour approach (for MAR)
knn_imputation <- impute(vsn, fun = "knn", rowmax = 0.9)
```

```{r visualize-imputed-pca}
#| eval = recompute || ! file.exists( file = filename)

colnames(knn_imputation) <- colData(knn_imputation)$label[match(colnames(knn_imputation), colData(knn_imputation)$ID)]

df_impute <- get_df_wide(knn_imputation) %>%
  select(-c("name", "ID", "imputed", "num_NAs")) %>%
  select(Accession, everything())

mat <- as.matrix(na.omit(df_impute[, -1]))
colnames(mat) <- samples$Sample[match(colnames(mat), samples$SampleID)]
```

**Save imputation**

```{r save-imputation}
save( file = filename, vsn, knn_imputation, df_impute)
```

### Identifying sources of unwanted variation 

It seems, that the imputation didn't affect the two main clusters that can be
seen in the PCA. After the imputation, we have proteomics data without any `NA`s
in it, and hence can apply the `sva` package to derive the surrogate variables
that capture the sources of unwanted variation.

Therefore, we will estimate the number of surrogate variables that we should
address with the `num.sv()` function.

Once we have this estimate, we can finally run the identification of hidden
variance by means of the `sva()` method.

```{r sva-filename}
filename <- file.path(RdataDir, "03_proteomics_liver_corrected.rda",
  fsep = .Platform$file.sep
)

if (!recompute && file.exists(filename)) {
  load(file = filename)
}
```

To run the `sva` method, we specify the model matrix to account for the
different treatment groups.

```{r run-sva}
#| eval = recompute || ! file.exists( file = filename)

## Prepare the sva run
samples_sub <- samples[match(colnames(df_impute)[-1], samples$SampleID), ]
mod <- model.matrix(~Treatment_group, data = samples_sub)
mod0 <- model.matrix(~1, data = samples_sub)

## estimate the number of surrogate variables
n.sv <- num.sv(mat, mod, method = "leek")

svobj <- sva(mat, mod, mod0, n.sv = n.sv)

save(file = filename, samples_sub, mod, mod0, svobj)
```

To check if the method succeeded in capturing the variance that caused the PCA
in any of the surrogate variables, we will plot the PCA with colors accoring to
the `sva` results.

```{r plot-pca-sva, fig.align='center', fig.cap='PCA of the imputed data measured in the liver samples. The top500 variable proteins were used for this PCA. Colors reflect the surrogate variable of factor 1 as it was determined by sva in the previous step.', warning=FALSE}
## plot the pca with sva-based coloring
## again use the top 500 variable proteins (same list as before)
pc <- prcomp(t(mat[select_ids, ]))
percentVar <- round(pc$sdev^2 / sum(pc$sdev^2) * 100, digits = 2)
pca_data <- data.frame(samples = rownames(pc$x), PC1 = pc$x[, 1], PC2 = pc$x[, 2])

pca_data$sv1 <- svobj$sv[, 1]


plt <- ggplot(pca_data, aes(x = PC1, y = PC2, color = sv1)) +
  geom_point(size = 3) +
  xlab(paste0("PC1: ", percentVar[1], "% variance")) +
  ylab(paste0("PC2: ", percentVar[2], "% variance")) +
  ggtitle(paste0("PCA of proteomics data ( ", nrow(mat[select_ids, ]), " variable proteins)")) +
  theme_bw()

plt
```

### Remove batch effect

It seems that surrogate variable 1 precisely covers the variance that is
responsible for the PCA clustering. Similar to the thyroid transcriptome data,
we can make use of the `limma` package to remove this batch effect and get a
nicer PCA.

```{r file-removed-batch-effect}
filename <- file.path(RdataDir, "04_proteomics_liver_removed_batch_effect.rda",
  fsep = .Platform$file.sep
)

if (!recompute && file.exists(filename)) {
  load(file = filename)
}
```

```{r remove-batch-effect}
#| eval = recompute || ! file.exists( file = filename)

mat_corrected <- removeBatchEffect(mat, covariates = svobj$sv[, 1], design = mod)

save(file = filename, mat_corrected)
```

Let's have a look at the final PCA with the corrected data:

```{r pca-without-batch-effect, fig.align='center', fig.cap='PCA of the imputed liver data with additional batch effect correction based on the surrogate variable of factor 1. Colors reflect the treatments while shapes denote different time points', warning=FALSE}
rv <- matrixStats::rowVars(mat_corrected)
select_ids <- order(rv, decreasing = TRUE)[seq_len(min(nTop, length(rv)))]
plt <- plot_pca(mat_corrected[select_ids, ])
plt
```

After imputation and batch correction, we get a much clearer separation of the
samples in the different treatment groups. Samples with high dose PTU and
Phenytoin treatment are now clearly separated from controls, low dose
treatments, and recovered samples.


### Save data for multi-omics integration

Prepare the imputed but not corrected proteomics data to be included our `MOFA2`
integration. This might help to get rid of the error we got from the gene set
enrichment which can be done with `MOFA2`. Using the unimputed data leads to an
error during the calculation of a protein-wise correlation matrix due to the
massive amounts of `NA`s in the data.

```{r save-imputed-data-as-tibble}
filename <- file.path(RdataDir, "01_liver_proteomics_vsn_imputed.rda")

liver_proteomics_vsn_imputed <- as_tibble(df_impute)

save(liver_proteomics_vsn_imputed, file = filename)
```

Finally, we will also save the corrected proteomics data to be included in our
multi-omics integration procedures.

```{r save-corrected-data-as-tibble}
filename <- file.path(RdataDir, "01_liver_proteomics_vsn_corrected.rda")

liver_proteomics_vsn_corrected <- as_tibble(mat_corrected) %>%
  mutate(Accession = liver_proteomics_vsn_imputed$Accession) %>%
  select(Accession, everything())
colnames(liver_proteomics_vsn_corrected) <- colnames(liver_proteomics_vsn_imputed)

save(liver_proteomics_vsn_corrected, file = filename)
```

### Create `MOFA2` object

Here, we utilize the **batch corrected** and **variance stabilized** liver proteomics
data set.


As for transcriptomics and metabolomics, we need to re-format the proteomics
data.

In the preprocessed proteomics data, we have already pre-filtered
features such that those proteins with less than three measurements in any
sample have been removed. The data is furthermore variance stabilized. That
means we only need to re-shape the data to be in the correct format.


```{r reshape-liver-proteomics}
filename <- file.path(RdataDir, "04_liver_proteomics_corrected_input.rda",
  fsep = .Platform$file.sep
)

if (recompute || !file.exists(filename)) {
  prot <- liver_proteomics_vsn_corrected %>%
    gather(key = "sample", value = "value", -Accession) %>%
    mutate(group = gsub(".*.G", "", sample)) %>%
    mutate(view = "Proteomics") %>%
    dplyr::rename("feature" = Accession) %>%
    select(feature, view, sample, value, group) %>%
    as.data.frame()

  save(prot, file = filename)
} else {
  load(file = filename)
}

rm(liver_proteomics_vsn_corrected)
```

# R Session

```{r session}
sessionInfo()
```












