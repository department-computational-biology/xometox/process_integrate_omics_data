---
title: "Multi-omics data integration with covariates"
subtitle: "MEFISTO - Liver samples - Phenytoin"
author: "Sebastian Canzler"
date: "June 18th, 2024"
output:
  bookdown::html_document2:
    toc: true
    toc_float: true
    number_sections: true
    theme: united 
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

# Aim

This markdown file summarizes the first steps to integrate multiple omics layers
with several covariates from the clinical and pathological side in the XomeTox
project.

The model contains the following omics data sets:

- Transcriptomics
- Short transcriptomics
- Tissue metabolomics
- Plasma metabolomics
- Proteomics
- Phosphoproteomics


Furtermore, the model contains only liver samples: controls and phenytoin treated.

# Reproducibility

To reproduce the analysis please use the following singularity container: `rocker_multiomics_analysis:latest.statmod.sif`

Or download from docker hub: `docker://boll3/rocker_multiomics:latest`

Digest: `64936504`

You can reproduce this Rmarkdown file and the figure by running:

```{r render-rmarkdown, eval = FALSE, engine='bash'}
cd /path/to/repo/
singularity exec /path/to/container/rocker_multiomics_analysis\:latest.statmod.sif \
  Rscript -e '{rmarkdown::render("03_MEFISTO_model_liver_Phenytoin.Rmd")'
```


# Set-up

## Local (R) environment

<p align="right"><a href="#top">&uarr; Back to top</a></p>

Here, all directories are set to the correct paths of this project:

```{r set-directories, echo = FALSE}
#####################################
# CHANGE WORKING DIRECTORY HERE !!! #
#####################################
my_wd <- "/path/to/workingdir/"
my_wd <- "/home/canzler/git-canzler/projects/2022_multi_omics_paper_scripts"
setwd(my_wd)


data_dir <- "/home/canzler/git-canzler/projects/2022_multi_omics_paper_scripts/DATA/MEFISTO/03_liver_models"
####################################
# CHANGE OUTPUT DIRECTORY HERE !!! #
####################################
# for storing .Rdata files
RdataDir <- file.path("/path/to/directory", "RData", fsep = .Platform$file.sep)
RdataDir <- file.path(data_dir, "RData", fsep = .Platform$file.sep)
if (!dir.exists(RdataDir)) {
  dir.create(RdataDir)
}


###################################
# CHANGE INPUT DIRECTORY HERE !!! #
###################################
# for storing input files
indir <- file.path("/path/to/directory", "input", fsep = .Platform$file.sep)
indir <- file.path(data_dir, "input", fsep = .Platform$file.sep)
if (!dir.exists(indir)) {
  dir.create(indir)
}


```

* Working directory: ```r my_wd```
* Data directory on EVE: ```r data_dir```
* RData storage: ```r RdataDir```

Load utils file for additional functions:

```{r include-helper-functions}
# include MOFA/MEFISTO helper functions
source("utils.R")
```


Necessary libraries to run this analysis are the following:

```{r load-libraries, warning=FALSE, message=FALSE}
library("MOFA2")
library("reticulate")
library("reshape2")
library("readxl")
library("knitr")
library("dplyr")
library("tidyverse")
library("magrittr")
library("xtable")
```



## Variables

```{r set-variables}
recompute <- 0
```

## Conflict resolution

As usual, there are several packages that provide functions with identical
names. Since we are mainly interested in a function of a particular package, we
will clarify this conflict right at the beginning.

**Please note** that functions with the same name from other packages must be
called with their package name as namespace, for example
```AnnotationDbi::select()```.

```{r conflicted}

select <- dplyr::select
filter <- dplyr::filter
rename <- dplyr::rename
group_by <- dplyr::group_by

```


## Data collection

Multi-omics data integration is based on previous data generation and
preprocessing of the individual omics modalities that shall be used.

**Transcriptomics**

Here we make use of the filtered and transformed data set
that was compiled in the transcriptomics preprocessing workflow:

[01_transcriptomics_processing.Rmd](https://codebase.helmholtz.cloud/department-computational-biology/xometox/process_integrate_omics_data/-/blob/master/01_transcriptomics_processing.html?ref_type=heads)

**Short RNA-seq**

The raw read counts have been measured by IGH. After batch correction
using the sva package we applied a variance stabilizing transformation.

The workflow can be found in file [01_short_Transcriptome_processing.Rmd](https://codebase.helmholtz.cloud/department-computational-biology/xometox/process_integrate_omics_data/-/blob/master/01_short_transcriptomics_processing.html?ref_type=heads).


**Plasma metabolomics**

The plasma metabolomics part of the multi-omics study was conducted by Metanomics
GmbH in Berlin.

Prior to the integration in our multi-omics model, we applied a variance
stabilizing transformation to the data. The whole workflow can be seen here: 
[01_Plasma_metabolome_processing.Rmd](https://codebase.helmholtz.cloud/department-computational-biology/xometox/process_integrate_omics_data/-/blob/master/01_Plasma_metabolomics_processing.html?ref_type=heads)

**Tissue metabolomics**

For the tissue metabolomics part of the multi-omics study we used the Biocrates
MxP® Quant 500 kits. 

Again, we run a variance stabilizing transformation prior to model integration.
The respective Rmarkdown workflow is [01_Tissue_metabolome_processing.Rmd](https://codebase.helmholtz.cloud/department-computational-biology/xometox/process_integrate_omics_data/-/blob/master/01_Tissue_metabolomics_processing.Rmd?ref_type=heads)


**Proteomics**

The normalized proteomics intensities underwent three normalization procedure to ensure comparability of each treatment group:

1) _Total peptide normalization._ - This is an TMT labelling batch normalization to ensure a comparability within a single TMT batch.
2) _mean normalization._ This normalization is needed to allow for comparisons accross multiple TMT batches.
3) Variance stabilizing transformation

The precise workflow can be found here: [01_Proteome_processing.Rmd](https://codebase.helmholtz.cloud/department-computational-biology/xometox/process_integrate_omics_data/-/blob/master/01_proteomics_processing.Rmd?ref_type=heads)


**Phosphoproteomics**

The raw IRS-normalized intensies have been corrected using the `sva` package and
subsequently variance stabilized.

The workflow can be found here: [01_Phosphoproteome_processing.Rmd](https://codebase.helmholtz.cloud/department-computational-biology/xometox/process_integrate_omics_data/-/blob/master/01_phosphoproteomics_processing.Rmd?ref_type=heads).


# Load data sets


## Transcriptomics

According to the authors the best way to create a MOFA model is to use long data.frames.
The data.frame should contain the columns `sample`, `feature`, `view`, `group`, and `value`.

```{r transform-transcriptome}

filename <- file.path( indir, "01_transcriptomic_input_liver_phenytoin.rda",
                       fsep = .Platform$file.sep)

load( file = filename)

## use only the batch corrected and subsetted/filtered data set
trans <- trans_sub
trans$view <- "Transcriptomics"

## remove the remaining two data sets
rm( trans_sub)
```


**Summary transcriptomics input**

* Number of samples: ```r trans %>% dplyr::distinct( sample) %>% nrow()```
* Genes in transcriptomics dataset: ```r trans %>% filter( sample == "L.A2.G1") %>% nrow()```


## Plasma metabolomics data

Load the plasma metabolomics data:

```{r load-plasma-metabolomics}
filename <- file.path( indir, "02_plasma_metabolomics_input_liver.rda",
                       fsep = .Platform$file.sep)
load( file = filename)
```

In the preprocessed plasma metabolomics data, we have already the pre-filtered
samples and filtered features such that those metabolites with less than three
measurements in any sample have been removed. The data is furthermore variance
stabilized. 

**Summary plasma metabolomics**

* Number of samples: ```r plasma %>% dplyr::distinct( sample) %>% nrow()```
* Number of metabolites: ```r plasma %>% filter( sample == "L.A52.G6") %>% nrow()```


## Tissue metabolomics data2

Load the tissue metabolomics data:

```{r load-tissue-metabolomics}
filename <- file.path( indir, "03_liver_metabolomics_input.rda",
                       fsep = .Platform$file.sep)
load( file = filename)
```

In the preprocessed tissue metabolomics data, we have already pre-filtered
features such that those metabolites with less than three measurements in any
sample have been removed. The data is furthermore variance stabilized. 


**Summary thyroid metabolomics**

* Number of samples: ```r meta %>% dplyr::distinct( sample) %>% nrow()```
* Number of metabolites: ```r meta %>% filter( sample == "L.A52.G6") %>% nrow()```


## Proteomics data

As for transcriptomics and metabolomics, we can already load the re-formatted proteomics
data.

```{r load-proteomics}
filename <- file.path( indir, "04_liver_proteomics_corrected_input.rda",
                       fsep = .Platform$file.sep)
load( file = filename)
```

In the preprocessed tissue metabolomics data, we have already pre-filtered
features such that those metabolites with less than three measurements in any
sample have been removed. The data is furthermore variance stabilized. 

**Summary thyroid proteomics**

* Number of samples: ```r prot %>% dplyr::distinct( sample) %>% nrow()```
* Number of proteins: ```r prot %>% filter( sample == "L.A52.G6") %>% nrow()```


## Short RNA-seq data

As for transcriptomics and metabolomics, we can already load the re-formatted miRNA data set.

```{r load-short-RNAseq}
filename <- file.path( indir, "05_liver_short_RNAseq_input.rda",
                       fsep = .Platform$file.sep)
load( file = filename)
```

In the preprocessed short transcriptomics data, we have already pre-filtered
features such that those miRNAs with missing values in any sample
have been removed. The data is furthermore variance stabilized.

**Summary liver miRNAs**

* Number of samples: ```r miRNA %>% dplyr::distinct( sample) %>% nrow()```
* Number of miRNAs: ```r miRNA %>% filter( sample == "L.A52.G6") %>% nrow()```


## Phosphoproteomics data

As for transcriptomics and metabolomics, we can already load the re-formatted phosphoproteomics
data.

```{r load-phosphoproteomics}
filename <- file.path( indir, "06_liver_phosphoproteomics_input.rda",
                       fsep = .Platform$file.sep)
load( file = filename)
```

In the preprocessed phosphoproteomics data, we have already pre-filtered
features such that those modification sites with missing values in any sample
have been removed. The data is furthermore variance stabilized.

**Summary liver phosphoproteomics**

* Number of samples: ```r phospho %>% dplyr::distinct( sample) %>% nrow()```
* Number of phosphorylation sites: ```r phospho %>% filter( sample == "L.A52.G6") %>% nrow()```



# Build MOFA model

Before building the model, we simply need to bind the omics layer data.frames
row by row. 

In this first exploration of the data, we subset the data to the PTU treatment
groups, hence we select the `Treatment_groups` `1,2,3,6,7,8,11,12,13`.

## Create dataset

```{r untrained-mofa-file}
filename <- file.path( RdataDir, "01_liver_mefisto_model_untrained_Phenytoin.rda", 
                       fsep = .Platform$file.sep)

groups_to_select <- c(1,2,3,6,7,8,11,12,13)

```

```{r prepare-dataframe}
#| eval = recompute || ! file.exists( filename)

df <- rbind( trans, prot, meta, plasma, miRNA, phospho) %>%
  filter( group %in% groups_to_select) %>%
  droplevels()

```


We set the ```group``` variable to one, meaning that we want to
analyse all samples with respect to their variablity between different groups.

```{r one-group-design}
#| eval = recompute || ! file.exists( filename)

df$group <- 1

```


Now it's time to build the model:

```{r create-mofa-model, message=FALSE, results='hide'}
#| eval = recompute || ! file.exists( filename)

mofa <- create_mofa( df)

```


## Include covariates

Load the covariates from preprocessed file.

```{r load-covariates-in-file}

covariates_file <- file.path(indir, "01_liver_Phenytoin_covariates.rda")

load( file = covariates_file)

```

The following covariates will be included in the model:

```{r show-covariates, results='asis'}

covariates %>% mutate(Group = gsub(".*G", "", sample)) %>%
  filter(Group %in% groups_to_select) %>%
  filter(sample %in% unique(trans$sample)) %>%
  pivot_wider(names_from = covariate, values_from = value) %>%
  mutate(Rel_Liver_weight = round(Rel_Liver_weight, digits = 2)) %>%
  mutate(Rel_Thyroid_weight = round(Rel_Thyroid_weight, digits = 2)) %>%
  DT::datatable(options = list(scrollX=TRUE), filter = 'top')

```

Include the covariates in the `MEFISTO` model.

```{r apply-covariates-to-mefisto-model}
#| eval = recompute || ! file.exists( filename)

## Apply all designed covariates to the model.
mofa <- set_covariates( mofa, covariates = covariates)

```


```{r save-mofa-objects}

if( recompute || !file.exists( filename)){
  
  save( mofa, file = filename)
  
} else{
  
  load( file = filename)
  
}

```


## Model overview

Now that we successfully created a `MEFISTO` model, we can have
a look a the different dimensions across the four omics layers as well as
their presence of each sample across all those layers.

A nice overview can be generated by the ```plot_data_overview()``` function:

```{r print-mofa}
print(mofa)
plot_data_overview(object = mofa, covariate = NULL)
```

## Set model options

In principle, there are plenty of options that can be set. For this first
exploration, we mainly stick to the default options with the possibility to
change them for another run.

### Define data options

```{r data-options}

data_opts <- get_default_data_options( mofa)
data_opts$scale_views <- TRUE
head( data_opts)

```

### Define model options

The default number of factors that shall be trained is ```15```. Here we set
this number to a smaller value with 5.

```{r set-model-options}
model_opts <- get_default_model_options( mofa)
model_opts$num_factors <- 5
head( model_opts)
```


### Define Training options

```{r set-training options}

train_opts <- get_default_training_options( mofa)
head( train_opts)

```

### Define MEFISTO options

```{r mefisto-options}
mefisto_opts <- get_default_mefisto_options( mofa)
```



# Train MOFA model

Finally, we can combine the untrained model with the set of options we defined
previously to actually run the training. All three trained models are stored in
the same ```rda``` file.

```{r trained-mofa-file}
filename <- file.path( RdataDir, "02_liver_complete_mefisto_model_trained_Phenytoin.rda", 
                       fsep = .Platform$file.sep)
```

```{r prepare-model}
#| eval = recompute || !file.exists( filename)

mofa <- prepare_mofa(
  object = mofa,
  data_options = data_opts,
  model_options = model_opts,
  training_options = train_opts,
  mefisto_options = mefisto_opts
)

```


```{r train-model}

if( recompute || ! file.exists( filename)){

  outfile = file.path(tempdir(),"model.hdf5")
  mofa.trained <- run_mofa( mofa, outfile, use_basilisk = FALSE)
  
  save( mofa.trained, file = filename)
  
} else{
  
  load( file = filename)
  
}

```

## Exploratory visualizations

After the model was fit, we can have a look at the captured variance in each of
the three models. From this basic plot, we can easily get a first impression if
```MOFA2``` was able to capture variability across multiple omics layer or not.

```{r plot_model}
plot_var_explained(mofa.trained, plot_total = TRUE)
plot_factor_cor( mofa.trained)
```


# Session

```{r session}
sessionInfo()
```







