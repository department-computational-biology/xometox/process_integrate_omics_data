---
title: "Analysis of differentially altered features"
subtitle: "Phosphoproteomics data"
author: "Sebastian Canzler"
date: "June 13th, 2024"
output:
  bookdown::html_document2:
    toc: true
    toc_float: true
    number_sections: true
    theme: united  
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```


This markdown file steps in after the mean normalization procedures to calculate
the differential expression and fold changes that will be used in the multiGSEA
pathway enrichment.


# Reproducibility

To reproduce the analysis please use the following singularity container: `rocker_multiomics_analysis:latest.sif`

Or download from docker hub: `docker://boll3/rocker_multiomics:latest`
Digest: `db12fdf`


This `.Rmd` file that is the basis of this report has been rendered via:

```{r render, eval=FALSE}
rmarkdown::render("02_phosphoproteomics_DEPs.Rmd", output_format = "html_document")
```


# Set-up

<p align="right"><a href="#top">&uarr; Back to top</a></p>

## Local (R) environment


Here, all directories are set to the correct paths of this project:

## Local (R) environment


Here, all directories are set to the correct paths of this project:

```{r set-directories, echo = FALSE}

#####################################
# CHANGE WORKING DIRECTORY HERE !!! #
#####################################
my_wd <- "/path/to/workingdir/"
my_wd <- "/home/canzler/git-canzler/projects/2022_multi_omics_paper_scripts"
setwd(my_wd)


####################################
# CHANGE OUTPUT DIRECTORY HERE !!! #
####################################
data_dir <- "path/to/datadir"
data_dir <- "/home/canzler/git-canzler/projects/2022_multi_omics_paper_scripts/DATA/phosphoproteomics/02_differentially_expressed_proteins"


###################################
# CHANGE RDATA DIRECTORY HERE !!! #
###################################
RdataDir <- file.path(data_dir, "RData", fsep = .Platform$file.sep)
if (!dir.exists(RdataDir)) {
  dir.create(RdataDir)
}


###################################
# CHANGE INPUT DIRECTORY HERE !!! #
###################################
indir <- file.path(data_dir, "input", fsep = .Platform$file.sep)
if (!dir.exists(indir)) {
  dir.create(indir)
}

```


* Working directory: ```r my_wd```
* Data directory on EVE: ```r data_dir```
* RData storage: ```r RdataDir```


Necessary libraries to run this analysis are the following:


```{r load-libraries, warning=FALSE, message=FALSE}

# Bioconductor stuff
library(DEP)
library(limma)
library(AnnotationDbi)
library(org.Rn.eg.db)

# CRAN stuff
library(tidyverse)
library(magrittr)
library(SummarizedExperiment)
library(EnhancedVolcano)
library(scales)

```


## Variables

```{r set-variables}

recompute <- 0
FDR <- 0.01

```


## Functions

```{r functions}

#' Adjust DEP calculated p-values using BH
#' 
#' @param DFrame RowData of DEP calculate differential expression
#' 
#' @return DFrame with BH-adjusted p-values
adjust_DEP_pvalues <- function(df){
  
  pval_cols <- grep("p.val", colnames(df))
  
  for (i in pval_cols){
    df[,i-1] <- p.adjust(df[,i], method = "BH")
  }
  
  return(df)
}

```

## Conflict resolution

As usual, there are several packages that provide functions with identical
names. Since we are mainly interested in a function of a particular package, we
will clarify this conflict right at the beginning.

**Please note** that functions with the same name from other packages must be
called with their package name as namespace, for example
```AnnotationDbi::select()```.

```{r conflicted}

select <- dplyr::select
filter <- dplyr::filter
rename <- dplyr::rename
group_by <- dplyr::group_by

```


# Load phosphoproteomics data sets

We will calculated the differential expression for the liver tissue in this Rmd
file. 

## Load liver data

Here, we load the imputed and corrected liver proteomics data sets.

```{r load-liver-data}

filename <- file.path( indir, "04_liver_phosphoproteomics_corrected_vsn_se.rda", 
                       fsep = .Platform$file.sep)
load( file = filename)

```



# Differential expression analysis

## Contrasts

Here, we run simple pairwise contrasts, comparing treatments against their respective controls:

**PTU**

- Controls **vs** low dose PTU @ 2 weeks
- Controls **vs** high dose PTU @ 2 weeks
- Controls **vs** low dose PTU @ 4 weeks
- Controls **vs** high dose PTU @ 4 weeks
- Controls **vs** low dose PTU @ 4 weeks + 2 weeks recovery
- Controls **vs** high dose PTU @ 4 weeks + 2 weeks recovery
- Low dose PTU @ 4 weeks **vs** low dose PTU @ 4 weeks + 2 weeks recovery
- High dose PTU @ 4 weeks **vs**  high dose PTU @ 4 weeks + 2 weeks recovery

**Phenytoin**

- Controls **vs** low dose Phenytoin @ 2 weeks
- Controls **vs** high dose Phenytoin @ 2 weeks
- Controls **vs** low dose Phenytoin @ 4 weeks
- Controls **vs** high dose Phenytoin @ 4 weeks
- Controls **vs** low dose Phenytoin @ 4 weeks + 2 weeks recovery
- Controls **vs** high dose Phenytoin @ 4 weeks + 2 weeks recovery
- Low dose Phenytoin @ 4 weeks **vs** low dose Phenytoin @ 4 weeks + 2 weeks recovery
- High dose Phenytoin @ 4 weeks **vs**  high dose Phenytoin @ 4 weeks + 2 weeks recovery


```{r contrasts}

contrasts <- c( "X2_vs_X1", "X3_vs_X1", "X4_vs_X1", "X5_vs_X1",
                "X7_vs_X6", "X8_vs_X6", "X9_vs_X6", "X10_vs_X6",
                "X12_vs_X11", "X13_vs_X11", "X14_vs_X11", "X15_vs_X11",
                "X12_vs_X7", "X13_vs_X8", "X14_vs_X9", "X15_vs_X10")

```

## Differentially abundant phosphorylation sites in liver

To calculate the differentially abundant phosphosites, we use the packages `DEP`
an `limma`. The function `test_diff` performs a differential enrichment test
based on linear models and empirical Bayes statistics using limma. False
Discovery Rates are originally estimated using `fdrtool`. However, we changed
this and implemented an adjustment using Benjamini-Hochberg.

```{r dea-liver-data, message=FALSE, warning=FALSE}

filename <- file.path( RdataDir, "03_liver_phosphoproteomics_dea_results.rda")

if( recompute || ! file.exists( filename)){
  diff_liver <- test_diff( liver_phosphoproteomics_corrected_vsn_se, type = "manual",
                          test = contrasts)
  
  # change FDR calculation to use BH adjustment
  rowData(diff_liver) <- adjust_DEP_pvalues(rowData(diff_liver))
  
  dep_liver <- add_rejections( diff_liver, alpha = 0.01, lfc = log2(1))
  
  res_liver <- get_results( dep_liver)
  
  res_liver$Protein <- gsub("_.*", "", res_liver$ID)
  
  annot <- AnnotationDbi::select(org.Rn.eg.db, unique(res_liver$Protein), columns = "SYMBOL", keytype = "UNIPROT") %>% 
  as_tibble() %>%
  rename("Protein" = UNIPROT)

  res_liver <- left_join(res_liver, annot) %>% 
    mutate(SYMBOL=tolower(SYMBOL))
  
  save( res_liver, file = filename)
  
} else{
  
  load(filename)
  
}
```


### Short Exposure - Phenytoin 

We plot the results of the contrasts **X3** (high dose Phenytoin - 2 Weeks) vs **X1**
(controls - 2 weeks).

```{r df-volcano-highdose-phenytoin-2weeks, fig.align='center', fig.cap="Valcano plot showing the results of a differential analysis on phosphorylation sites in proteins in liver samples. High dose treated Phenytoin samples (2 weeks) were tested against their respective controls (also 2 Weeks).", fig.width=13, fig.height=10}


df <- data.frame( ratio = res_liver$X3_vs_X1_ratio, 
                  padj = res_liver$X3_vs_X1_p.adj,
                  name = res_liver$name,
                  Symbol = res_liver$SYMBOL)

EnhancedVolcano( toptable = df, x = 'ratio', y = 'padj', pCutoff = FDR, 
                 lab = df$name, ylab = "-log10( adj.pvalue)",
                 title = "Differentially alterered phosphorylation sites in liver proteins",
                 subtitle = "High dose Phenytoin (2 Weeks) vs Controls (2 Weeks)")

```

In the following table, I will summarize those phosphorylation sites that have
been found to be significantly altered upon the high dose Phenytoin treatment as shown
in Figure \@ref(fig:df-volcano-highdose-phenytoin-2weeks).

```{r table-results-2weeks, results='asis', echo=FALSE}
df %>% filter( padj < FDR) %>% 
  dplyr::rename("Protein" = name, "Fold_change" = ratio, "Adj.Pvalue" = padj) %>% 
  select(Protein, Symbol, Fold_change, Adj.Pvalue) %>%
  arrange(Adj.Pvalue) %>% 
  mutate(Adj.Pvalue = scientific(Adj.Pvalue, digits = 4)) %>%
  DT::datatable( rownames = FALSE, caption = "Differentially abundant liver phosphorylation sites after high dose Phenytoin treatment (2 Weeks). This table summarizes all sites with an adjusted p-value smaller than 0.01. This accounts for all sites that are colored in red AND blue in the previous volcano plot.")
```

### Long Exposure - Phenytoin

We plot the results of the contrasts **X8** (high dose PTU - 4 Weeks) vs **X6**
(controls - 4 weeks).

```{r df-volcano-highdose-phenytoin, fig.align='center', fig.cap="Valcano plot showing the results of a differential analysis of phosphorylation sites in liver samples. High dose treated Phenytoin samples (4 weeks) were tested against their respective controls (also 4 Weeks).", fig.width=13, fig.height=10}

df <- data.frame( ratio = res_liver$X8_vs_X6_ratio, 
                  padj = res_liver$X8_vs_X6_p.adj,
                  name = res_liver$name,
                  Symbol = res_liver$SYMBOL)

EnhancedVolcano( toptable = df, x = 'ratio', y = 'padj', pCutoff = FDR, 
                 lab = df$name, ylab = "-log10( adj.pvalue)",
                 title = "Differentially alterered phosphorylation sites in liver proteins",
                 subtitle = "High dose Phenytoin (4 Weeks) vs Controls (4 Weeks)")

```

### Long Exposure + Recovery - Phenytoin

We plot the results of the contrasts **X13** (high dose Phenytoin - 6 Weeks) vs **X11**
(controls - 6 weeks).

```{r df-volcano-highdose-phneytoin-6weeks, fig.align='center', fig.cap="Valcano plot showing the results of a differential analysis of phosphorylation sites in proteins in liver samples. High dose treated Phenytoin samples (6 weeks) were tested against their respective controls (also 6 Weeks).", fig.width=13, fig.height=10}

df <- data.frame( ratio = res_liver$X13_vs_X11_ratio, 
                  padj = res_liver$X13_vs_X11_p.adj,
                  name = res_liver$name,
                  Symbol = res_liver$SYMBOL)

EnhancedVolcano( toptable = df, x = 'ratio', y = 'padj', pCutoff = FDR, 
                 lab = df$name, ylab = "-log10( adj.pvalue)",
                 title = "Differentially alterered phosphorylation sites in liver proteins",
                 subtitle = "High dose Phenytoin (6 Weeks) vs Controls (6 Weeks)")

```



### Short Exposure - PTU 

We plot the results of the contrasts **X5** (high dose PTU - 2 Weeks) vs **X1**
(controls - 2 weeks).

```{r df-volcano-highdose-ptu-2weeks, fig.align='center', fig.cap="Valcano plot showing the results of a differential analysis of phosphorylation sites in liver samples. High dose treated PTU samples (2 weeks) were tested against their respective controls (also 2 Weeks).", fig.width=13, fig.height=10}


df <- data.frame( ratio = res_liver$X5_vs_X1_ratio, 
                  padj = res_liver$X5_vs_X1_p.adj,
                  name = res_liver$name,
                  Symbol = res_liver$SYMBOL)

EnhancedVolcano( toptable = df, x = 'ratio', y = 'padj', pCutoff = FDR, 
                 lab = df$name, ylab = "-log10( adj.pvalue)",
                 title = "Differentially alterered phosphorylation sites in liver proteins",
                 subtitle = "High dose PTU (2 Weeks) vs Controls (2 Weeks)")

```

### Long Exposure - PTU

We plot the results of the contrasts **X10** (high dose PTU - 4 Weeks) vs **X6**
(controls - 4 weeks).

```{r df-volcano-highdose-ptu, fig.align='center', fig.cap="Valcano plot showing the results of a differential analysis of phosphorylation sites in proteins in liver samples. High dose treated PTU samples (4 weeks) were tested against their respective controls (also 4 Weeks).", fig.width=13, fig.height=10}

df <- data.frame( ratio = res_liver$X10_vs_X6_ratio, 
                  padj = res_liver$X10_vs_X6_p.adj,
                  name = res_liver$name,
                  Symbol = res_liver$SYMBOL)

EnhancedVolcano( toptable = df, x = 'ratio', y = 'padj', pCutoff = FDR, 
                 lab = df$name, ylab = "-log10( adj.pvalue)",
                 title = "Differentially alterered phosphorylation sites in liver proteins",
                 subtitle = "High dose PTU (4 Weeks) vs Controls (4 Weeks)")

```


### Long Exposure + Recovery - PTU

We plot the results of the contrasts **X15** (high dose PTU - 6 Weeks) vs **X11**
(controls - 6 weeks).

```{r df-volcano-highdose-ptu-6weeks, fig.align='center', fig.cap="Valcano plot showing the results of a differential analysis of phosphorylation sites on proteins in liver samples. High dose treated PTU samples (6 weeks) were tested against their respective controls (also 6 Weeks).", fig.width=13, fig.height=10}

df <- data.frame( ratio = res_liver$X15_vs_X11_ratio, 
                  padj = res_liver$X15_vs_X11_p.adj,
                  name = res_liver$name,
                  Symbol = res_liver$SYMBOL)

EnhancedVolcano( toptable = df, x = 'ratio', y = 'padj', pCutoff = FDR, 
                 lab = df$name, ylab = "-log10( adj.pvalue)",
                 title = "Differentially alterered phsophorylation sites in liver proteins",
                 subtitle = "High dose PTU (6 Weeks) vs Controls (6 Weeks)")

```


# R Session

```{r session-info}
sessionInfo()
```
